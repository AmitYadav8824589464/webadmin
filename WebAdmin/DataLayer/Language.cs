﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class LanguageList
    {
        public List<Language> LangList { get; set; }
    }
    public class Language
    {
        public string Action { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Language Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Language Code is Required")]
        public string Code { get; set; }

        public string IsActive { get; set; }

        public Language AddLanguage(Language obj)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<Language>("Usp_Language", new { obj.Action, obj.Id, obj.Name, obj.Code, obj.IsActive }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new Language();
                }
            }
            return obj;
        }

        public Language GetLanguage(int Id)
        {
            Language obj = new Language();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<Language>("Usp_Language", new { @Action = "GetById", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (obj == null)
                {

                    obj = new Language();
                }
            }
            return obj;
        }

        public LanguageList GetLanguageList()
        {
            LanguageList obj = new LanguageList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple("Usp_Language", new { @Action = "Get" }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.LangList = data.Read<Language>().ToList();
                }
                else
                {
                    obj = new LanguageList();
                }
            }
            return obj;
        }
    }
}
