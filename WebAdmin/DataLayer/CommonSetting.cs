﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace DataLayer
{
    public static class CommonSetting
    {
        public static string ProjectName = WebConfigurationManager.AppSettings["ProjectName"];
        public static string Patient { get { return "P0001"; } }
        public static string Doctor { get { return "D0001"; } }
        public static string SiteUrl { get { return WebConfigurationManager.AppSettings["siteUrl"]; } }
        public static string ProfilePhoto { get { return SiteUrl + "/uploads/profile/"; } }
        public static string CountryFlag { get { return SiteUrl + "/uploads/flag/"; } }
        public static string AvatarUrl { get { return SiteUrl + "/uploads/avatar/"; } }
        public static string SpecialityPhoto { get { return SiteUrl + "/uploads/speciality/"; } }
        public static string StaticPage { get { return SiteUrl + "/uploads/staticpage/"; } }
        public static double HoursDiff { get { return 5.5; } }

        public static string constr = ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString;

        public static string Role { get; set; }
        public static string Name { get; set; }


    }
}
