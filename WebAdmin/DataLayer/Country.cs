﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class CountryList
    {
        public List<Country> List { get; set; }
    }
    public class Country
    {
        public string Action { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Iso Code is Required")]
        public string IsoCode { get; set; }

        [Required(ErrorMessage = "Country Name is Required")]
        public string Name { get; set; }

        public string RegEnabled { get; set; }
        public string LoginEnabled { get; set; }

        [Required(ErrorMessage = "Minimum Age is Required")]
        public string MinAge { get; set; }

        [Required(ErrorMessage = "Dial Code is Required")]
        public string DialCode { get; set; }

        [Required(ErrorMessage = "Phone Format is Required")]
        public string PhoneFormat { get; set; }


        public Country AddCountry(Country obj)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<Country>("Usp_Country", new { obj.Action, obj.Id, obj.Name, obj.IsoCode, obj.RegEnabled, obj.LoginEnabled, obj.MinAge, obj.DialCode, obj.PhoneFormat }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new Country();
                }
            }
            return obj;
        }

        public Country GetCountry(int Id)
        {
            Country obj = new Country();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<Country>("Usp_Country", new { @Action = "GetById", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (obj == null)
                {

                    obj = new Country();
                }
            }
            return obj;
        }

        public CountryList GetCountryList()
        {
            CountryList obj = new CountryList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple("Usp_Country", new { @Action = "Get" }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.List = data.Read<Country>().ToList();
                }
                else
                {
                    obj = new CountryList();
                }
            }
            return obj;
        }

    }
}
