﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DataLayer
{
    public class GoogleLocation
    {
        public List<string> GetCountryName(string Lat, string Lng)
        {
            List<string> objCountry = new List<string>();
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Lat + "," + Lng + "&key=AIzaSyCQ0Dj0h2vqHGNXNT42gIEJKoJP_4rGIdA";
            var client = new WebClient();
            var method = "POST"; // If your endpoint expects a GET then do it.
            var parameters = new NameValueCollection();
            var response_data = System.Text.Encoding.ASCII.GetString(client.UploadValues(url, method, parameters));
            GoogleLocationApiResponce objResponce = new JavaScriptSerializer().Deserialize<GoogleLocationApiResponce>(response_data.ToString());


            if (objResponce.results.Count > 1)
            {
                var Address = objResponce.results[2].address_components;
                string countryName = Address[Address.Count - 2].long_name;
                string countryNameShort = Address[Address.Count - 2].short_name.ToLower();
                objCountry.Add(countryName);
                objCountry.Add(countryNameShort);
            }
            return objCountry;
        }

    }
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }



    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }

        public string place_id { get; set; }
        public List<string> types { get; set; }

        public Components components { get; set; }
    }

    public class GoogleLocationApiResponce
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}
