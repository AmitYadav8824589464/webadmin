﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class SystemConfigList
    {
        public List<SystemConfig> LogList { get; set; }
    }
    public class SystemConfig
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Main Configuration is Required")]
        public string MainConfig { get; set; }

        [Required(ErrorMessage = "Custom Options is Required")]
        public string CustomOptions { get; set; }

        [Required(ErrorMessage = "Rules is Required")]
        public string Rules { get; set; }

        [Required(ErrorMessage = "Main Configuration is Required")]
        public string Hosts { get; set; }

        [Required(ErrorMessage = "Main Configuration is Required")]
        public string EndPoints { get; set; }

        public string Action { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }
        public string CreateDate { get; set; }

        public List<SystemConfig> ConfigList { get; set; }


        public SystemConfig AddSystemConfig(SystemConfig obj)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<SystemConfig>("Usp_SystemConfig", new { obj.Action, obj.MainConfig, obj.CustomOptions, obj.Rules, obj.Hosts, obj.EndPoints }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new SystemConfig();
                }
            }
            return obj;
        }
        public SystemConfig RevertSystemConfig(string Key)
        {
            SystemConfig obj = new SystemConfig();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<SystemConfig>("Usp_SystemConfig", new { @Action = "Revert", @Key = Key }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new SystemConfig();
                }
            }
            return obj;
        }
        public SystemConfig GetSystemConfig(int Id)
        {
            SystemConfig obj = new SystemConfig();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<SystemConfig>("Usp_SystemConfig", new { @Action = "Get", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (obj == null)
                {

                    obj = new SystemConfig();
                }
            }
            return obj;
        }
        public SystemConfigList GetSystemLog()
        {
            SystemConfigList obj = new SystemConfigList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple("Usp_SystemConfig", new { @Action = "Log" }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.LogList = data.Read<SystemConfig>().ToList();
                }
                else
                {
                    obj = new SystemConfigList();
                }
            }
            return obj;
        }
    }
}
