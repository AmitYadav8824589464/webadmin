﻿using Dapper;
using DataLayer;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataLayer
{
    public class AdminLoginList
    {
        public List<AdminLogin> GameUsers { get; set; }
        public int totalCount { get; set; }
    }

    public class AdminLogin
    {
        #region Proerties
        public string Action { get; set; }
        public int Id { get; set; }
        public string ParentId { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        [StringLength(12, MinimumLength = 4, ErrorMessage = "Password Length Should be 4 to 12.")]
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }

        [Required(ErrorMessage = "User Name is required")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Commision is required")]
        [RegularExpression(@"^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$", ErrorMessage = "Enter Valid Commission")]

        public decimal Commission { get; set; }
        //[MaxLength(14)]       
        [RegularExpression(@"^([0-9]{10,15})$", ErrorMessage = "MOBILE NUMBER MUST BE BETWEEN 10 TO 15 DIGIT")]
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Role { get; set; }
        public bool rememberMe { get; set; }
        public string Msg { get; set; }
        public string CreateDate { get; set; }
        public string LastLoginDate { get; set; }
        public decimal Gold { get; set; }
        public string PlayerId { get; set; }
        public long TotalUser { get; set; }
        public decimal TotalCommission { get; set; }
        public string ParentRole { get; set; }
        public string NameWithId { get; set; }

        public string LoginId { get; set; }

        public decimal Debit { get; set; }
        public decimal Credit { get; set; }

        public string SenderLoginId { get; set; }
        public string ReceiverLoginId { get; set; }

        public int UserId { get; set; }
        public int SenderId { get; set; }
        public string SenderRole { get; set; }
        public int ReceiverId { get; set; }
        public string ReceiverRole { get; set; }
        public decimal Amount_Dr { get; set; }
        [Required(ErrorMessage = "Amount is required")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "Enter Valid Amount")]
        public decimal Amount_Cr { get; set; }
        public decimal ProfitLoss { get; set; }
        public decimal NetBalance { get; set; }
        public string Type { get; set; }

        public string Comms { get; set; }

        public int FromUserId { get; set; }
        public int Cost { get; set; }
        public string GiftType { get; set; }
        public string RoomName { get; set; }

        public string AgntParent { get; set; }
        public string MstrParent { get; set; }


        [Required(ErrorMessage = "Amount is required")]
        [RegularExpression(@"^-?[1-9]\d*$", ErrorMessage = "Enter Valid Amount")]
        public decimal SendGold { get; set; }

        public decimal TotalChips { get; set; }

        public decimal UpperLevelCommission { get; set; }

        public string EncryptId { get; set; }

        public int CommandTimeOut = 2000;
        #endregion


        public AdminLogin Create(AdminLogin obj, string sp)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj.password = obj.password ?? "";
                obj = conn.Query<AdminLogin>(sp, new { obj.ParentId, obj.Address, obj.Email, obj.Name, obj.MobileNo, obj.Commission, obj.Id, obj.UserName, obj.password, obj.Remarks, obj.Role, obj.Action }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new AdminLogin();
                }
            }
            return obj;
        }

        public static bool ActiveDeactive(int Id = 0, string sp = "")
        {
            AdminLogin obj = new AdminLogin();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<AdminLogin>(sp, new { @Action = "ActDeAct", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return obj.IsActive;
            }
        }

        public static bool BlockUser(int Id = 0, string sp = "")
        {
            AdminLogin obj = new AdminLogin();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<AdminLogin>(sp, new { @Action = "Delete", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return obj.IsDelete;
            }
        }

        public string PasswordReset(int Id = 0, string sp = "")
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.Query<AdminLogin>(sp, new { @Action = "ResetPassword", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                return obj.password;
            }
        }

        public AdminLogin GetCreaterData(int Id, string sp)
        {
            AdminLogin obj = new AdminLogin();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<AdminLogin>(sp, new { @Action = "DataById", @Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (obj == null)
                {

                    obj = new AdminLogin();
                }
            }
            return obj;
        }

        public AdminLoginList GetCreaterList(int pageIndex, int pageSize, string UserId, string playerID, string Emailid, string Name, string Role, string ParentId, string sp, string ParentRole, string LoginId)
        {
            AdminLoginList obj = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple(sp, new { @pageIndex = pageIndex, @pageSize = pageSize, @Action = "Show", @UserId = UserId, Role, Name, @Email = Emailid, ParentId, @ParentRole = ParentRole, @LoginId = LoginId }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.GameUsers = data.Read<AdminLogin>().ToList();
                    obj.totalCount = data.Read<int>().FirstOrDefault();
                }
                else
                {
                    obj = new AdminLoginList();
                }
            }
            return obj;
        }
        public AdminLoginList GetGoldTransferUserlist(int pageIndex, int pageSize, string UserId, string playerID, string Emailid, string Name, string Role, string ParentId, string sp, string ParentRole, string LoginId)
        {
            AdminLoginList obj = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple(sp, new { @pageIndex = pageIndex, @pageSize = pageSize, @Action = "GoldUserShow", @UserId = UserId, Role, Name, @Email = Emailid, ParentId, @ParentRole = ParentRole, @LoginId = LoginId }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.GameUsers = data.Read<AdminLogin>().ToList();
                    obj.totalCount = data.Read<int>().FirstOrDefault();
                }
                else
                {
                    obj = new AdminLoginList();
                }
            }
            return obj;
        }

        public AdminLoginList GetGoldTransferUserlistofadmin(int pageIndex, int pageSize, string UserId, string Role, string ParentId, string sp, string ParentRole)
        {
            AdminLoginList obj = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple(sp, new { @pageIndex = pageIndex, @pageSize = pageSize, @Action = "masterofadmin", @UserId = UserId, Role, ParentId, @ParentRole = ParentRole }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.GameUsers = data.Read<AdminLogin>().ToList();
                    //       obj.totalCount = data.Read<int>().FirstOrDefault();
                }
                else
                {
                    obj = new AdminLoginList();
                }
            }
            return obj;
        }


        public List<AdminLogin> GetUserId(string Role, string ParentId, string sp)
        {
            AdminLoginList obj = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                return conn.Query<AdminLogin>(sp, new { @Action = "Show", Role, ParentId }, commandType: CommandType.StoredProcedure).ToList();

            }

        }
        public AdminLoginList ListofAdminMaster(int pageIndex, int pageSize, string Role)
        {
            AdminLoginList obj = new AdminLoginList();

            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.Query<AdminLogin>(@"USP_MastersOfAdmin", new { @pageIndex = pageIndex, pageSize = pageSize, @Role = Role }, commandType: CommandType.StoredProcedure).ToList();

                if (data != null)
                {
                    obj.GameUsers = data.ToList();
                    //       obj.totalCount = data.Read<int>().FirstOrDefault();
                }
                else
                {
                    obj = new AdminLoginList();
                }

            }


            return obj;
        }


        public Login Login(Login objLogin)
        {
            Login obj = new Login();

            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<Login>(@"USP_AdminLogin", new { @LoginId = objLogin.Loginid, @Password = objLogin.password }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new Login();
                }
                else
                {
                    obj.rememberMe = objLogin.rememberMe;
                }
            }


            return obj;
        }


        public List<AdminLogin> GetUserRole(AdminLogin objadmin)
        {
            List<AdminLogin> obj = new List<AdminLogin>();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<AdminLogin>("select id,role from tbl_userrole where isactive=1 and id >(select id from tbl_userrole where role= '" + objadmin.Role.ToString() + "')", commandType: CommandType.Text).ToList();
            }
            return obj;
        }

        public int GetRoleId(string Role)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                return conn.Query<int>("select id from tbl_userrole where isactive=1 and  role= '" + Role.ToString() + "'", commandType: CommandType.Text).FirstOrDefault();
            }

        }




     

        public List<AdminLogin> GetMasterList(string UserId)
        {
            List<AdminLogin> obj = new List<AdminLogin>();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple("USP_AdminCreate", new { @Action = "DataById", @Id = UserId }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj = data.Read<AdminLogin>().ToList();
                }
                else
                {
                    obj = new List<AdminLogin>();
                }
            }
            return obj;
        }

        public AdminLoginList GetLevelDetail(string UserId = "", string Role = "")
        {
            AdminLoginList getaccountsummurylist = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("USP_AccountSummary", new { @Action = "LevelDetail", @UserId = UserId, @Role = Role }, commandType: CommandType.StoredProcedure, commandTimeout: CommandTimeOut);
                getaccountsummurylist.GameUsers = obj.Read<AdminLogin>().ToList();
                return getaccountsummurylist;
            }
        }


        public AdminLoginList GetProfitLossList(int pageIndex, int pageSize, string UserId = "", string Role = "", string name = "", decimal gold = 0, string Status = "")
        {
            AdminLoginList getaccountsummurylist = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("USP_AccountSummary", new { @Action = "ProfitLoss", @PageIndex = pageIndex, @pageSize = pageSize, @UserId = UserId, @Role = Role, @name = name, @gold = gold, @status = Status }, commandType: CommandType.StoredProcedure, commandTimeout: CommandTimeOut);
                getaccountsummurylist.GameUsers = obj.Read<AdminLogin>().ToList();
                getaccountsummurylist.totalCount = obj.Read<int>().FirstOrDefault();
                return getaccountsummurylist;
            }
        }

        public AdminLoginList GetEmojiChatList(int pageIndex, int pageSize, string UserId = "", string Role = "", string name = "")
        {
            AdminLoginList getaccountsummurylist = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("USP_EmojiChatSummary", new { @Action = "GetList", @PageIndex = pageIndex, @pageSize = pageSize, @UserId = UserId, @Role = Role, @name = name }, commandType: CommandType.StoredProcedure, commandTimeout: CommandTimeOut);
                getaccountsummurylist.GameUsers = obj.Read<AdminLogin>().ToList();
                getaccountsummurylist.totalCount = obj.Read<int>().FirstOrDefault();
                return getaccountsummurylist;
            }
        }


        public AdminLoginList GetAccountSummury(int pageIndex, int pageSize, string UserId = "", string Role = "", string name = "", decimal gold = 0, string Status = "")
        {
            AdminLoginList getaccountsummurylist = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("USP_AccountSummary", new { @Action = "AccountSummaryDetail", @PageIndex = pageIndex, @pageSize = pageSize, @UserId = UserId, @Role = Role, @name = name, @gold = gold, @status = Status }, commandType: CommandType.StoredProcedure, commandTimeout: CommandTimeOut);
                getaccountsummurylist.GameUsers = obj.Read<AdminLogin>().ToList();
                getaccountsummurylist.totalCount = obj.Read<int>().FirstOrDefault();
                return getaccountsummurylist;
            }
        }

        public AdminLoginList GetAccountLedger(int PageIndex, int pageSize, string UserId, string Role, string ReceiverId, string ReceiverRole, string FromDate, string ToDate)
        {
            AdminLoginList getaccountledgerlist = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("USP_AccountSummary", new { @Action = "AccountLedgerDetail", PageIndex = PageIndex, pageSize = pageSize, @UserId = UserId, @Role = Role, @ReceiverId = ReceiverId, @ReceiverRole = ReceiverRole, FromDate, ToDate }, commandType: CommandType.StoredProcedure);
                getaccountledgerlist.GameUsers = obj.Read<AdminLogin>().ToList();
                getaccountledgerlist.totalCount = obj.Read<int>().FirstOrDefault();
            }
            return getaccountledgerlist;
        }


        public AdminLoginList GetUserDetail(string UserId, string Role, string ParentId, string sp, string ParentRole)
        {
            AdminLoginList obj = new AdminLoginList();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var data = conn.QueryMultiple(sp, new { @Action = "Show", @Id = UserId, Role, ParentId, @ParentRole = ParentRole }, commandType: CommandType.StoredProcedure);
                if (data != null)
                {
                    obj.GameUsers = data.Read<AdminLogin>().ToList();
                }
                else
                {
                    obj = new AdminLoginList();
                }
            }
            return obj;
        }
        public AdminLogin Set_AccountSettlement(AdminLogin obj)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                obj = conn.Query<AdminLogin>("USP_AccountSummary", new { @Action = "AccountSettlement", obj.UserId, obj.Role, obj.ReceiverId, obj.ReceiverRole, @Gold = obj.SendGold, @Password = obj.password }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (obj == null)
                {
                    obj = new AdminLogin();
                }
            }
            return obj;
        }

    }

    public class Login
    {
        [Required(ErrorMessage = "User Name is required")]
        public string Loginid { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }
        public bool rememberMe { get; set; }
        public bool IsActive { get; set; }
        public long userId { get; set; }
        public int Id { get; set; }
        public string Role { get; set; }
    }





    //public AdminLoginList Getmastererofadmin(Login objLogin)
    //{
    //    AdminLoginList obj = new AdminLoginList();

    //    using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
    //    {
    //        obj = conn.Query<AdminLoginList>(@"USP_AdminLogin", new { @LoginId = objLogin.email, @Password = objLogin.password }, commandType: CommandType.StoredProcedure).ToList();

    //        if (obj == null)
    //        {
    //            obj = new AdminLoginList();
    //        }
    //        else
    //        {

    //        }
    //    }


    //    return obj;
    //}

    public class ChangePassword
    {
        [Required(ErrorMessage = "Current Password is required")]
        public string CurrentPassword { get; set; }
        [StringLength(20, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 5)]
        [Required(ErrorMessage = "New Password is required")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Confirm Password is required")]
        [Compare("NewPassword", ErrorMessage = "New Password & Confirm Password do not match")]
        public string ConfirmPassword { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
    }

}
