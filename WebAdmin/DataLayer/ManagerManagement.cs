﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataLayer;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace DataLayer
{
    public class ManagerManagement
    {
        #region Properties
        public int? mgrId { get; set; }
        [Required(ErrorMessage = "Manager Name Required")]
        public string mgrName { get; set; }
      
        [Required(ErrorMessage = "Email Is Required ")]
      
        public string Email { get; set; }
       
        public string Address { get; set; }

        public string City { get; set; }
        [Required(ErrorMessage ="Phone No. is Required")]
        public string PhoneNo { get; set; }
        public bool isActive { get; set; }
        public decimal coins { get; set;}
        public decimal amount { get; set; }
        public string Msg { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Muserid { get; set; }
        #endregion


       
        public ManagerManagement managerinsert(ManagerManagement obj)

        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))


            {

                var obj1 = conn.Query<string>("sp_manager", new
                {
                    Action = "Insert",
                    name = obj.mgrName,
                    email = obj.Email,
                    address = obj.Address
                    ,
                    MobileNo = obj.PhoneNo
                ,
                   City=obj.City,

                    id = obj.mgrId
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                
                    return obj;

            }
        }


        public IList<ManagerManagement> managerlist()
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj1 = conn.Query<ManagerManagement>("sp_manager", new { Action = "View" }, commandType: CommandType.StoredProcedure).ToList();
                return obj1;
            }
        }

        public ManagerManagement managerlistforedit(Int32 id)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                ManagerManagement manager = new ManagerManagement();
                manager = conn.Query<ManagerManagement>("sp_manager", new { Action = "show", id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                // subcoordinator = obj1[0];


                return manager;
            }
        }


        public static bool mgrdeactivedata(int id=0)
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.Query<ManagerManagement>("sp_manager", new { Action = "isdeactive", id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return obj.isActive;
            }
            return false;
        }

     


        public Managers managerPaging(int pageIndex1, int pageSize1)
        {
            Managers Manager = new Managers();
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                var obj = conn.QueryMultiple("[sp_manager_pageindex]", new { PageIndex = pageIndex1, pageSize = pageSize1 }, commandType: CommandType.StoredProcedure);
                Manager.Mgrlist = obj.Read<ManagerManagement>().ToList();
                Manager.totalCount = obj.Read<string>().FirstOrDefault();
            }
            return Manager;
        }
    }

    #region coin management
  
#endregion
public class Managers
    {
        public List<ManagerManagement> Mgrlist { get; set; }
        public string totalCount { get; set; }
    }


}




