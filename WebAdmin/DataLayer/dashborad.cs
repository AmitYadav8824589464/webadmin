﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class dashborad
    {

        public decimal golcharge { get; set; }
        public decimal matchfee { get; set; }
        public decimal totalamount { get; set; }
        public decimal TotalCommission { get; set; }
        public Int64 OnlineTotalTables { get; set; }
        public Int64 OnlinePlayers { get; set; }
        public Int64 TodayGames { get; set; }
        public Int64 WeeklyGames { get; set; }
        public Int64 TotalMaster { get; set; }



        public dashborad gettotalearning(string ParentId = "", string ParentRole = "")
        {
            using (IDbConnection conn = new SqlConnection(CommonSetting.constr))
            {
                dashborad obj = new dashborad();

                var amount = conn.QueryMultiple("sp_gettoaladminearning",new { ParentId, ParentRole }, commandType: CommandType.StoredProcedure);

                obj.golcharge = amount.Read<decimal>().FirstOrDefault();

                obj.matchfee = amount.Read<decimal>().FirstOrDefault();
                obj.TotalCommission = amount.Read<decimal>().FirstOrDefault();
                obj.totalamount = obj.golcharge + obj.matchfee;
                obj.OnlineTotalTables = amount.Read<Int64>().FirstOrDefault();
                obj.OnlinePlayers = amount.Read<Int64>().FirstOrDefault();
                obj.TodayGames = amount.Read<Int64>().FirstOrDefault();
                obj.WeeklyGames = amount.Read<Int64>().FirstOrDefault();
                obj.TotalMaster = amount.Read<Int64>().FirstOrDefault();


                return obj;

            }
        }
    }
}
