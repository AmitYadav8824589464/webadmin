﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class EnumMethod
    {
        public enum AppointmentStatus
        {
            Pending = 1,
            Completed = 2,
            Rejected = 3,
            Approved = 4,
            Notresponded=5
        }

        public enum Notficationflag
        {
            Booking = 1,
            Accepted = 2,
            Rejected = 3,
            Schduled = 4
        }
    }

}
