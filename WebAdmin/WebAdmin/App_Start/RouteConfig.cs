﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebAdmin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Reports
            routes.MapRoute(name: "ExportReportList", url: "Admin/Report/List/Export", defaults: new { controller = "Admin", action = "ExportReportList" });
            #endregion

            #region Redeem Codes
            routes.MapRoute(name: "RedeemCodeList", url: "Admin/Redeem/Code/List", defaults: new { controller = "Admin", action = "RedeemCodeList" });
            routes.MapRoute(name: "GenerateRedeemCode", url: "Admin/Redeem/Code/Generate", defaults: new { controller = "Admin", action = "GenerateRedeemCode" });
            routes.MapRoute(name: "AddRedeemCode", url: "Admin/Redeem/Code/Add", defaults: new { controller = "Admin", action = "AddRedeemCode" });
            #endregion

            #region Rakeback
            routes.MapRoute(name: "RakebackPercentConfig", url: "Admin/Rakeback/Config", defaults: new { controller = "Admin", action = "RakebackPercentConfig" });
            routes.MapRoute(name: "RakebackReport", url: "Admin/Rakeback/List", defaults: new { controller = "Admin", action = "RakebackReport" });
            #endregion

            #region Promotions Management
            routes.MapRoute(name: "Promotions", url: "Admin/Promotions/List", defaults: new { controller = "Admin", action = "Promotions" });
            routes.MapRoute(name: "AddPromotion", url: "Admin/Promotions/Add", defaults: new { controller = "Admin", action = "AddPromotion" });
            #endregion

            #region Prohibited Users
            routes.MapRoute(name: "ProhibitedIPList", url: "Admin/Prohibited/IP/List", defaults: new { controller = "Admin", action = "ProhibitedIPList" });
            routes.MapRoute(name: "ProhibitedDeviceList", url: "Admin/Prohibited/Device/List", defaults: new { controller = "Admin", action = "ProhibitedDeviceList" });
            routes.MapRoute(name: "AddProhibitedIP", url: "Admin/Prohibited/IP/Add", defaults: new { controller = "Admin", action = "AddProhibitedIP" });
            routes.MapRoute(name: "AddProhibitedDevice", url: "Admin/Prohibited/Device/Add", defaults: new { controller = "Admin", action = "AddProhibitedDevice" });
            #endregion

            #region Payment Systems
            routes.MapRoute(name: "PaymentSettings", url: "Admin/Payment/Settings", defaults: new { controller = "Admin", action = "PaymentSettings" });
            routes.MapRoute(name: "PaymentLimitsList", url: "Admin/Payment/LimitsList", defaults: new { controller = "Admin", action = "PaymentLimitsList" });
            routes.MapRoute(name: "LiteCoin", url: "Admin/Payment/LiteCoin", defaults: new { controller = "Admin", action = "LiteCoin" });
            routes.MapRoute(name: "LimitSettings", url: "Admin/Payment/LimitSettings", defaults: new { controller = "Admin", action = "LimitSettings" });
            routes.MapRoute(name: "FastPay", url: "Admin/Payment/FastPay", defaults: new { controller = "Admin", action = "FastPay" });
            routes.MapRoute(name: "EcorePay", url: "Admin/Payment/EcorePay", defaults: new { controller = "Admin", action = "EcorePay" });
            routes.MapRoute(name: "EasyPayDirectPayment", url: "Admin/Payment/EasyPayDirect", defaults: new { controller = "Admin", action = "EasyPayDirectPayment" });
            routes.MapRoute(name: "DefaultLimitValues", url: "Admin/Payment/DefaultLimitValues", defaults: new { controller = "Admin", action = "DefaultLimitValues" });
            routes.MapRoute(name: "Bitcoin", url: "Admin/Payment/Bitcoin", defaults: new { controller = "Admin", action = "Bitcoin" });
            #endregion

            #region Notification Management
            routes.MapRoute(name: "AddEmailTemplate", url: "Admin/EmailTemplate/Add", defaults: new { controller = "Admin", action = "AddEmailTemplate" });
            routes.MapRoute(name: "EmailTemplateList", url: "Admin/EmailTemplate/List", defaults: new { controller = "Admin", action = "EmailTemplateList" });
            routes.MapRoute(name: "NotificationAdd", url: "Admin/Notification/Add", defaults: new { controller = "Admin", action = "NotificationAdd" });
            routes.MapRoute(name: "NotificationList", url: "Admin/Notification/List", defaults: new { controller = "Admin", action = "NotificationList" });
            #endregion

            #region In-App Purchases
            routes.MapRoute(name: "PurchaseList", url: "Admin/purchases/List", defaults: new { controller = "Admin", action = "PurchaseList" });
            routes.MapRoute(name: "PurchaseAdd", url: "Admin/purchases/Add", defaults: new { controller = "Admin", action = "PurchaseAdd" });
            routes.MapRoute(name: "ProductList", url: "Admin/purchases/Product/List", defaults: new { controller = "Admin", action = "ProductList" });
            routes.MapRoute(name: "ProductAdd", url: "Admin/purchases/Product/Add", defaults: new { controller = "Admin", action = "ProductAdd" });
            routes.MapRoute(name: "ProductPurchaseSettings", url: "Admin/purchases/Settings", defaults: new { controller = "Admin", action = "ProductPurchaseSettings" });
            #endregion

            #region Google Analytics
            routes.MapRoute(name: "G_Analytics", url: "Admin/ganalytics", defaults: new { controller = "Admin", action = "G_Analytics" });
            routes.MapRoute(name: "DevicesReports", url: "Admin/ganalytics/DevicesReports", defaults: new { controller = "Admin", action = "DevicesReports" });
            #endregion

            #region Deposit Bonus
            routes.MapRoute(name: "BonusInfo", url: "Admin/Bonus/Info", defaults: new { controller = "Admin", action = "BonusInfo" });
            routes.MapRoute(name: "BonusList", url: "Admin/Bonus/List", defaults: new { controller = "Admin", action = "BonusList" });
            routes.MapRoute(name: "BonusConfig", url: "Admin/Bonus/Config", defaults: new { controller = "Admin", action = "BonusConfig" });
            #endregion

            #region Clubs Management
            routes.MapRoute(name: "ClubEdit", url: "Admin/Club/Edit", defaults: new { controller = "Admin", action = "ClubEdit" });
            routes.MapRoute(name: "UnionList", url: "Admin/Club/List/Union", defaults: new { controller = "Admin", action = "UnionList" });
            routes.MapRoute(name: "ClubList", url: "Admin/Club/List", defaults: new { controller = "Admin", action = "ClubList" });
            routes.MapRoute(name: "ClubAdd", url: "Admin/Club/Add", defaults: new { controller = "Admin", action = "ClubAdd" });
            #endregion

            #region Casino Games
            routes.MapRoute(name: "Settings", url: "Admin/Casino/Place/Settings", defaults: new { controller = "Admin", action = "Settings" });
            routes.MapRoute(name: "ProviderList", url: "Admin/Casino/List/Provider", defaults: new { controller = "Admin", action = "ProviderList" });
            routes.MapRoute(name: "CategoriesList", url: "Admin/Casino/List/Categories", defaults: new { controller = "Admin", action = "CategoriesList" });
            routes.MapRoute(name: "CasinoGames", url: "Admin/Casino/List/Games", defaults: new { controller = "Admin", action = "CasinoGames" });
            #endregion

            #region Banner Management
            routes.MapRoute(name: "BannerPlaceEdit", url: "Admin/Banner/Place/Edit", defaults: new { controller = "Admin", action = "BannerPlaceEdit" });
            routes.MapRoute(name: "BannerEdit", url: "Admin/Banner/Edit", defaults: new { controller = "Admin", action = "BannerEdit" });
            routes.MapRoute(name: "BannerPlaceList", url: "Admin/Banner/Place/List", defaults: new { controller = "Admin", action = "BannerPlaceList" });
            routes.MapRoute(name: "AddBannerPlace", url: "Admin/Banner/Place/Add", defaults: new { controller = "Admin", action = "AddBannerPlace" });
            routes.MapRoute(name: "BannerList", url: "Admin/Banner/List", defaults: new { controller = "Admin", action = "BannerList" });
            routes.MapRoute(name: "BannerAdd", url: "Admin/Banner/Add", defaults: new { controller = "Admin", action = "BannerAdd" });
            #endregion

            #region Anti-Fraud
            routes.MapRoute(name: "TournamentsChipDumping", url: "Admin/AntiFraud/TournamentsChipDumping", defaults: new { controller = "Admin", action = "TournamentsChipDumping" });
            routes.MapRoute(name: "TeamBlockList", url: "Admin/AntiFraud/List/TeamBlock", defaults: new { controller = "Admin", action = "TeamBlockList" });
            routes.MapRoute(name: "NewPlayers", url: "Admin/AntiFraud/Players/New", defaults: new { controller = "Admin", action = "NewPlayers" });
            routes.MapRoute(name: "GameSearch", url: "Admin/AntiFraud/GameSearch", defaults: new { controller = "Admin", action = "GameSearch" });
            routes.MapRoute(name: "FraudIPList", url: "Admin/AntiFraud/List/FraudIP", defaults: new { controller = "Admin", action = "FraudIPList" });
            routes.MapRoute(name: "CollusionTournamentsGames", url: "Admin/AntiFraud/CollusionTournamentsGames", defaults: new { controller = "Admin", action = "CollusionTournamentsGames" });
            routes.MapRoute(name: "CollusionGamesList", url: "Admin/AntiFraud/List/CollusionGames", defaults: new { controller = "Admin", action = "CollusionGamesList" });
            routes.MapRoute(name: "ChipDumping", url: "Admin/AntiFraud/FraudGamePairs", defaults: new { controller = "Admin", action = "ChipDumping" });
            routes.MapRoute(name: "Bot", url: "Admin/AntiFraud/Bot", defaults: new { controller = "Admin", action = "Bot" });
            #endregion

            #region Actions Log
            routes.MapRoute(name: "AllChanges", url: "Admin/ActionLog/List", defaults: new { controller = "Admin", action = "AllChanges" });
            #endregion

            #region Report Generation
            routes.MapRoute(name: "ReportView", url: "Admin/ReportGen/View", defaults: new { controller = "Admin", action = "ReportView" });
            routes.MapRoute(name: "ReportList", url: "Admin/ReportGen/List", defaults: new { controller = "Admin", action = "ReportList" });
            routes.MapRoute(name: "AddReport", url: "Admin/ReportGen/Add", defaults: new { controller = "Admin", action = "AddReport" });
            #endregion

            #region Financial Reports
            routes.MapRoute(name: "AdHocReport", url: "Admin/Financial/Report/AdHoc", defaults: new { controller = "Admin", action = "AdHocReport" });
            routes.MapRoute(name: "JackpotReport", url: "Admin/Financial/Report/Jackpot", defaults: new { controller = "Admin", action = "JackpotReport" });
            routes.MapRoute(name: "MoneyInSystem", url: "Admin/Financial/Report/MoneyInSystem", defaults: new { controller = "Admin", action = "MoneyInSystem" });
            routes.MapRoute(name: "PlayerFinanceInsuranceReport", url: "Admin/Financial/Report/Player/Insurance", defaults: new { controller = "Admin", action = "PlayerFinanceInsuranceReport" });
            routes.MapRoute(name: "PlayerMoney", url: "Admin/Financial/Report/Player/Money", defaults: new { controller = "Admin", action = "PlayerMoney" });
            routes.MapRoute(name: "PlayerRake", url: "Admin/Financial/Report/Player/Rake", defaults: new { controller = "Admin", action = "PlayerRake" });
            routes.MapRoute(name: "TableRake", url: "Admin/Financial/Report/Table/Rake", defaults: new { controller = "Admin", action = "TableRake" });
            routes.MapRoute(name: "Rake", url: "Admin/Financial/Report/Rake", defaults: new { controller = "Admin", action = "Rake" });
            #endregion

            #region Transactions
            routes.MapRoute(name: "CreateTransaction", url: "Admin/Transactions/Create", defaults: new { controller = "Admin", action = "CreateTransaction" });
            routes.MapRoute(name: "TransactionDashboard", url: "Admin/Transactions/Dashboard", defaults: new { controller = "Admin", action = "TransactionDashboard" });
            routes.MapRoute(name: "ListCashOutPending", url: "Admin/Transactions/Cashout/Pending/List", defaults: new { controller = "Admin", action = "ListCashOutPending" });
            routes.MapRoute(name: "ListPending", url: "Admin/Transactions/Pending/List", defaults: new { controller = "Admin", action = "ListPending" });
            routes.MapRoute(name: "PlayerTransactions", url: "Admin/Transactions/Player", defaults: new { controller = "Admin", action = "PlayerTransactions" });
            routes.MapRoute(name: "ProcessedTransaction", url: "Admin/Transactions/Processed", defaults: new { controller = "Admin", action = "ProcessedTransaction" });
            routes.MapRoute(name: "SystemTransactions", url: "Admin/Transactions/System", defaults: new { controller = "Admin", action = "SystemTransactions" });
            routes.MapRoute(name: "TransactionInfo", url: "Admin/Transactions/Info", defaults: new { controller = "Admin", action = "TransactionInfo" });

            #endregion

            #region Jackpot
            routes.MapRoute(name: "JackPotAwardsHistory", url: "Admin/Jackpot/Awards/History", defaults: new { controller = "Admin", action = "JackPotAwardsHistory" });
            routes.MapRoute(name: "Jackpot", url: "Admin/Jackpot/Add", defaults: new { controller = "Admin", action = "Jackpot" });
            #endregion

            #region Tournaments
            routes.MapRoute(name: "ActiveTournaments", url: "Admin/Tournaments/Active", defaults: new { controller = "Admin", action = "ActiveTournaments" });
            routes.MapRoute(name: "AddTicketTemplate", url: "Admin/Tournaments/TicketTemplate/Add", defaults: new { controller = "Admin", action = "AddTicketTemplate" });
            routes.MapRoute(name: "TournamentDashboard", url: "Admin/Tournaments/Dashboard", defaults: new { controller = "Admin", action = "TournamentDashboard" });
            routes.MapRoute(name: "HiddenTicketTemplates", url: "Admin/Tournaments/TicketTemplates/Hidden", defaults: new { controller = "Admin", action = "HiddenTicketTemplates" });
            routes.MapRoute(name: "SpinList", url: "Admin/Tournaments/SpinList", defaults: new { controller = "Admin", action = "SpinList" });
            routes.MapRoute(name: "TournamentGames", url: "Admin/Tournaments/Games", defaults: new { controller = "Admin", action = "TournamentGames" });
            routes.MapRoute(name: "TournamentTables", url: "Admin/Tournaments/Tables", defaults: new { controller = "Admin", action = "TournamentTables" });
            routes.MapRoute(name: "TournamentTickets", url: "Admin/Tournaments/Tickets", defaults: new { controller = "Admin", action = "TournamentTickets" });
            routes.MapRoute(name: "TournamentTableList", url: "Admin/Tournaments/List/Table", defaults: new { controller = "Admin", action = "TournamentTableList" });
            routes.MapRoute(name: "TournamentAdd", url: "Admin/Tournaments/Add", defaults: new { controller = "Admin", action = "TournamentAdd" });
            routes.MapRoute(name: "TournamentHistory", url: "Admin/Tournaments/History", defaults: new { controller = "Admin", action = "TournamentHistory" });
            routes.MapRoute(name: "TournamentInfo", url: "Admin/Tournaments/Info", defaults: new { controller = "Admin", action = "TournamentInfo" });
            routes.MapRoute(name: "TournamentsStatistic", url: "Admin/Tournaments/Statistic", defaults: new { controller = "Admin", action = "TournamentsStatistic" });
            routes.MapRoute(name: "TournamentsList", url: "Admin/Tournaments/List", defaults: new { controller = "Admin", action = "TournamentsList" });
            #endregion

            #region Table
            routes.MapRoute(name: "ActiveTables", url: "Admin/Table/Active", defaults: new { controller = "Admin", action = "ActiveTables" });
            routes.MapRoute(name: "TableDashboard", url: "Admin/Table/Dashboard", defaults: new { controller = "Admin", action = "TableDashboard" });
            routes.MapRoute(name: "GameInfo", url: "Admin/Table/Game/Info", defaults: new { controller = "Admin", action = "GameInfo" });
            routes.MapRoute(name: "GameList", url: "Admin/Table/Game/List", defaults: new { controller = "Admin", action = "GameList" });
            routes.MapRoute(name: "RentReport", url: "Admin/Table/List/Rent", defaults: new { controller = "Admin", action = "RentReport" });
            routes.MapRoute(name: "TableChats", url: "Admin/Table/Chats", defaults: new { controller = "Admin", action = "TableChats" });
            routes.MapRoute(name: "TableAdd", url: "Admin/Table/Add", defaults: new { controller = "Admin", action = "TableAdd" });
            routes.MapRoute(name: "TableHistory", url: "Admin/Table/History", defaults: new { controller = "Admin", action = "TableHistory" });
            routes.MapRoute(name: "TableInfo", url: "Admin/Table/Info", defaults: new { controller = "Admin", action = "TableInfo" });
            routes.MapRoute(name: "TableList", url: "Admin/Table/List", defaults: new { controller = "Admin", action = "TableList" });

            #endregion

            #region Real Time Monitoring
            routes.MapRoute(name: "OnlinePlayerLocation", url: "Admin/Monitoring/OnlinePlayer/Location", defaults: new { controller = "Admin", action = "OnlinePlayerLocation" });
            routes.MapRoute(name: "OnlinePlayerList", url: "Admin/Monitoring/OnlinePlayer/List", defaults: new { controller = "Admin", action = "OnlinePlayerList" });
            routes.MapRoute(name: "AllTablesChatList", url: "Admin/Monitoring/AllTablesChat/List", defaults: new { controller = "Admin", action = "AllTablesChatList" });
            #endregion

            #region Backoffice Users
            routes.MapRoute(name: "BackOfficeUserRolePermissionList", url: "Admin/Backoffice/User/Role/Permission/List", defaults: new { controller = "Admin", action = "BackOfficeUserRolePermissionList" });
            routes.MapRoute(name: "BackOfficeUserRolePermissionAdd", url: "Admin/Backoffice/User/Role/Permission/Add", defaults: new { controller = "Admin", action = "BackOfficeUserRolePermissionAdd" });
            routes.MapRoute(name: "BackOfficeUserList", url: "Admin/Backoffice/User/List", defaults: new { controller = "Admin", action = "BackOfficeUserList" });
            routes.MapRoute(name: "AssignBackOfficeUser", url: "Admin/Backoffice/User/Role/Assign", defaults: new { controller = "Admin", action = "AssignBackOfficeUser" });
            routes.MapRoute(name: "CustomRoleList", url: "Admin/Backoffice/User/Role/List", defaults: new { controller = "Admin", action = "CustomRoleList" });
            routes.MapRoute(name: "AddCustomRole", url: "Admin/Backoffice/User/Role/Add", defaults: new { controller = "Admin", action = "AddCustomRole" });
            #endregion

            #region User
            routes.MapRoute(name: "UserRole", url: "Admin/User/Role", defaults: new { controller = "Admin", action = "UserRole" });
            routes.MapRoute(name: "UserPlayerDeviceList", url: "Admin/User/PlayerDeviceList", defaults: new { controller = "Admin", action = "UserPlayerDeviceList" });
            routes.MapRoute(name: "PlayerClubList", url: "Admin/User/PlayerClubList", defaults: new { controller = "Admin", action = "PlayerClubList" });
            routes.MapRoute(name: "UserPlayerSessionList", url: "Admin/User/PlayerSessionList", defaults: new { controller = "Admin", action = "UserPlayerSessionList" });
            routes.MapRoute(name: "UserPlayerTournamentList", url: "Admin/User/PlayerTournamentList", defaults: new { controller = "Admin", action = "UserPlayerTournamentList" });
            routes.MapRoute(name: "UserPlayerTableList", url: "Admin/User/PlayerTableList", defaults: new { controller = "Admin", action = "UserPlayerTableList" });
            routes.MapRoute(name: "UserPlayerTransactionList", url: "Admin/User/PlayerClubList", defaults: new { controller = "Admin", action = "UserPlayerTransactionList" });
            routes.MapRoute(name: "UserEdit", url: "Admin/User/Edit", defaults: new { controller = "Admin", action = "UserEdit" });
            routes.MapRoute(name: "UserList", url: "Admin/User/List", defaults: new { controller = "Admin", action = "UserList" });
            routes.MapRoute(name: "RegisteredUsers", url: "Admin/User/Registered", defaults: new { controller = "Admin", action = "RegisteredUsers" });
            routes.MapRoute(name: "PredefinedAvatar", url: "Admin/User/PredefinedAvatar", defaults: new { controller = "Admin", action = "PredefinedAvatar" });
            routes.MapRoute(name: "GroupPlayerView", url: "Admin/User/View/GroupPlayer", defaults: new { controller = "Admin", action = "GroupPlayerView" });
            routes.MapRoute(name: "GroupPlayerEdit", url: "Admin/User/Edit/GroupPlayer", defaults: new { controller = "Admin", action = "GroupPlayerEdit" });
            routes.MapRoute(name: "GroupPlayerAdd", url: "Admin/User/Add/GroupPlayer", defaults: new { controller = "Admin", action = "GroupPlayerAdd" });
            routes.MapRoute(name: "GroupPlayerList", url: "Admin/User/List/GroupPlayer", defaults: new { controller = "Admin", action = "GroupPlayerList" });
            routes.MapRoute(name: "ParticipationList", url: "Admin/User/List/Participation", defaults: new { controller = "Admin", action = "ParticipationList" });
            routes.MapRoute(name: "NotApprovedAvatar", url: "Admin/User/NotApprovedAvatar", defaults: new { controller = "Admin", action = "NotApprovedAvatar" });
            routes.MapRoute(name: "PlayerGameSummary", url: "Admin/User/Summary/PlayerGame", defaults: new { controller = "Admin", action = "PlayerGameSummary" });
            routes.MapRoute(name: "PlayerGameInfo", url: "Admin/User/Info/PlayerGame", defaults: new { controller = "Admin", action = "PlayerGameInfo" });
            routes.MapRoute(name: "PlayerDeviceList", url: "Admin/User/List/PlayerDevice", defaults: new { controller = "Admin", action = "PlayerDeviceList" });
            routes.MapRoute(name: "UserAdd", url: "Admin/User/Add", defaults: new { controller = "Admin", action = "UserAdd" });
            routes.MapRoute(name: "UserDashBoard", url: "Admin/DashBoard/User", defaults: new { controller = "Admin", action = "UserDashBoard" });
            #endregion

            #region Affiliate
            routes.MapRoute(name: "AffiliateProfit", url: "Admin/Affiliate/List/Profit", defaults: new { controller = "Admin", action = "AffiliateProfit" });
            routes.MapRoute(name: "PlayerInsuranceReport", url: "Admin/Affiliate/List/PlayerInsurance", defaults: new { controller = "Admin", action = "PlayerInsuranceReport" });
            routes.MapRoute(name: "PlayerReport", url: "Admin/Affiliate/List/Player", defaults: new { controller = "Admin", action = "PlayerReport" });


            routes.MapRoute(name: "PlayerSessionList", url: "Admin/Affiliate/List/PlayerSession", defaults: new { controller = "Admin", action = "AffiliateInfo" });
            routes.MapRoute(name: "PlayerTableList", url: "Admin/Affiliate/List/PlayerTable", defaults: new { controller = "Admin", action = "AffiliateInfo" });
            routes.MapRoute(name: "PlayerTournamentsList", url: "Admin/Affiliate/List/PlayerTournament", defaults: new { controller = "Admin", action = "AffiliateInfo" });
            routes.MapRoute(name: "PlayerTransactionList", url: "Admin/Affiliate/List/PlayerTransaction", defaults: new { controller = "Admin", action = "AffiliateInfo" });
            routes.MapRoute(name: "AffiliateInfo", url: "Admin/Affiliate/Info", defaults: new { controller = "Admin", action = "AffiliateInfo" });
            routes.MapRoute(name: "AffiliateEdit", url: "Admin/Affiliate/Edit", defaults: new { controller = "Admin", action = "AffiliateEdit" });
            routes.MapRoute(name: "AffiliateMake", url: "Admin/Affiliate/Make", defaults: new { controller = "Admin", action = "AffiliateMake" });
            routes.MapRoute(name: "AffiliateSettings", url: "Admin/Affiliate/Settings", defaults: new { controller = "Admin", action = "AffiliateSettings" });
            routes.MapRoute(name: "AffiliateAdd", url: "Admin/Affiliate/Add", defaults: new { controller = "Admin", action = "AffiliateAdd" });
            routes.MapRoute(name: "AffiliateList", url: "Admin/Affiliate/List", defaults: new { controller = "Admin", action = "AffiliateList" });
            #endregion

            #region Skin
            routes.MapRoute(name: "SkinMoneyReport", url: "Admin/Skin/MoneyReport", defaults: new { controller = "Admin", action = "SkinMoneyReport" });
            routes.MapRoute(name: "SkinAdd", url: "Admin/Skin/Add", defaults: new { controller = "Admin", action = "SkinAdd" });
            routes.MapRoute(name: "SkinList", url: "Admin/Skin/List", defaults: new { controller = "Admin", action = "SkinList" });
            #endregion

            #region Server
            routes.MapRoute(name: "MaintenanceMode", url: "Admin/Server/MaintenanceMode", defaults: new { controller = "Admin", action = "MaintenanceMode" });
            routes.MapRoute(name: "LobbyChat", url: "Admin/Server/Chat", defaults: new { controller = "Admin", action = "LobbyChat" });
            routes.MapRoute(name: "ServerAdd", url: "Admin/Server/Add", defaults: new { controller = "Admin", action = "ServerAdd" });
            routes.MapRoute(name: "ServerList", url: "Admin/Server/List", defaults: new { controller = "Admin", action = "ServerList" });
            #endregion

            #region Configuration
            routes.MapRoute(name: "PlayerLevelSettings", url: "Admin/Config/PlayerLevel/Settings", defaults: new { controller = "Admin", action = "PlayerLevelSettings" });
            routes.MapRoute(name: "PlayerLevelAdd", url: "Admin/Config/PlayerLevel/Add", defaults: new { controller = "Admin", action = "PlayerLevelAdd" });
            routes.MapRoute(name: "PlayerLevelList", url: "Admin/Config/PlayerLevel/List", defaults: new { controller = "Admin", action = "PlayerLevelList" });
            routes.MapRoute(name: "FastMessageAdd", url: "Admin/Config/FastMessage/Add", defaults: new { controller = "Admin", action = "FastMessageAdd" });
            routes.MapRoute(name: "FastMessageList", url: "Admin/Config/FastMessage/List", defaults: new { controller = "Admin", action = "FastMessageList" });
            routes.MapRoute(name: "ExchangeRateSettings", url: "Admin/Config/ExchangeRate/Update/Settings", defaults: new { controller = "Admin", action = "ExchangeRateSettings" });
            routes.MapRoute(name: "ExchangeRateAdd", url: "Admin/Config/ExchangeRate/Add", defaults: new { controller = "Admin", action = "ExchangeRateAdd" });
            routes.MapRoute(name: "ExchangeRateList", url: "Admin/Config/ExchangeRate/List", defaults: new { controller = "Admin", action = "ExchangeRateList" });
            routes.MapRoute(name: "CurrencyAdd", url: "Admin/Config/Currency/Add", defaults: new { controller = "Admin", action = "CurrencyAdd" });
            routes.MapRoute(name: "CurrencyList", url: "Admin/Config/Currency/List", defaults: new { controller = "Admin", action = "CurrencyList" });
            routes.MapRoute(name: "ClassificationAdd", url: "Admin/Config/Classification/Add", defaults: new { controller = "Admin", action = "ClassificationAdd" });
            routes.MapRoute(name: "ClassificationList", url: "Admin/Config/Classification/List", defaults: new { controller = "Admin", action = "ClassificationList" });

            routes.MapRoute(name: "CountryDelete", url: "Admin/Config/Country/Delete/{id}", defaults: new { controller = "Admin", action = "CountryDelete", id = UrlParameter.Optional });
            routes.MapRoute(name: "CountryEdit", url: "Admin/Config/Country/Edit/{id}", defaults: new { controller = "Admin", action = "CountryAdd", id = UrlParameter.Optional });
            routes.MapRoute(name: "CountryAdd", url: "Admin/Config/Country/Add", defaults: new { controller = "Admin", action = "CountryAdd" });
            routes.MapRoute(name: "CountryList", url: "Admin/Config/Country/List", defaults: new { controller = "Admin", action = "CountryList" });
           
            routes.MapRoute(name: "Localization", url: "Admin/Config/Localization", defaults: new { controller = "Admin", action = "Localization" });

            routes.MapRoute(name: "LanguageDelete", url: "Admin/Config/Language/Delete/{id}", defaults: new { controller = "Admin", action = "LanguageDelete", id = UrlParameter.Optional });
            routes.MapRoute(name: "LanguageEdit", url: "Admin/Config/Language/Edit/{id}", defaults: new { controller = "Admin", action = "LanguageAdd", id = UrlParameter.Optional });
            routes.MapRoute(name: "LanguageAdd", url: "Admin/Config/Language/Add", defaults: new { controller = "Admin", action = "LanguageAdd" });
            routes.MapRoute(name: "LanguageList", url: "Admin/Config/Language/List", defaults: new { controller = "Admin", action = "LanguageList" });

            routes.MapRoute(name: "SystemLog", url: "Admin/Config/System/Log", defaults: new { controller = "Admin", action = "SystemLog" });
            routes.MapRoute(name: "System", url: "Admin/Config/System/Edit", defaults: new { controller = "Admin", action = "System" });
            #endregion

            #region Admin
            routes.MapRoute(name: "DashBoard", url: "Admin/DashBoard", defaults: new { controller = "Admin", action = "DashBoard" });
            routes.MapRoute(name: "AdminLogout", url: "Admin/Logout", defaults: new { controller = "Admin", action = "Logout" });
            #endregion


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Admin", action = "DashBoard", id = UrlParameter.Optional }
            );
        }
    }
}
