﻿var listing = {
    setup: function () {
        $('body').off('click', '#butFilter').on('click', '#butFilter', listing.filter);
        $('body').off('click', '#butReset').on('click', '#butReset', listing.reset);

        $('body').on('ifChecked', '#rowCheckboxAll', function () {
            listing.toggleAll(true);
        });

        $('body').on('ifUnchecked', '#rowCheckboxAll', function () {
            listing.toggleAll(false);
        });

        $('.orderLink').off('click').on('click', listing.order);

        $(document).off('click', '.paginator-controls a').on('click', '.paginator-controls a', listing.page);

        $('.limit-links').off('change').on('change', listing.limit);

        $("#dynamicContent a.ajax-link").off('click').on('click', function (e) {
            e.preventDefault();
            var a = $(this);
            if (a.data('confirm') && !confirm(a.data('confirm'))) {
                return;
            }
            listing.ajaxLink(this);
        });
        $("#dynamicContent a.modal-link").off('click').on('click', function (e) {
            e.preventDefault();
            listing.modalLink(this);
        });

        $('.expanded').off('click').on('click', listing.onClickCollapsable);

        Array.prototype.slice.call(listing.getFilterFields()).forEach(function (e) {
            $(e).attr('onkeydown', 'javascript: if(event.keyCode == 13) { event.preventDefault(); listing.filter(event); }');
        });

        $(document).trigger("listingSetup");
    },

    addSetup: function (callback, appendBefore, skipNativeSetup) {
        appendBefore = appendBefore || false;
        skipNativeSetup = skipNativeSetup || false;

        if (skipNativeSetup) {
            listing.setup = callback;
            return;
        }

        var listingSetup = listing.setup;
        listing.setup = function () {
            if (appendBefore) {
                callback();
            }
            listingSetup();
            if (!appendBefore) {
                callback();
            }
        };
    },

    toggleAll: function (checked) {
        if (checked) {
            $('.rowCheckbox').iCheck('check');
        }
        else {
            $('.rowCheckbox').iCheck('uncheck');
        }
    },

    getFilterFields: function (event) {
        var wrapper, $table = event && (wrapper = $(event.target).closest('.content')).length ? wrapper : $(document);
        return $table.find('.filter');
    },

    ajaxLink: function (a) {
        throbeProcess.xhrGet({
            url: a.href,
            preventCache: true,
            handleAs: 'text',
            load: function () {
                listing.sendRequest({});
            }/*,
            error: function (error) {
                listing.sendRequest({});
            }*/
        });
    },

    modalForm: function (form) {
        var $form = $(form);
        throbeProcess.xhrPost({
            form: $form,
            handleAs: 'text',
            load: function (data) {
                listing.modalAjaxResponse(data, $form.data('modal-title') || $form.attr('title') || '');
            }
        });
    },

    modalLink: function (a) {
        var $a = $(a);
        throbeProcess.xhrGet({
            url: a.href,
            handleAs: 'text',
            load: function (data) {
                listing.modalAjaxResponse(data, $a.data('modal-title') || $a.attr('title') || '');
            }
        });
    },

    modalAjaxResponse: function (data, title) {
        var html = "<div class=\"modal fade\" id=\"modal-default\">\n" +
            "  <div class=\"modal-dialog\">\n" +
            "    <div class=\"modal-content\">\n" +
            "      <div class=\"modal-header\">\n" +
            "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n" +
            "          <span aria-hidden=\"true\">Ã—</span></button>\n" +
            "        <h4 class=\"modal-title\">" + title + "</h4>\n" +
            "      </div>\n" +
            "      <div class=\"modal-body\">\n" + data +
            "      </div>\n" +
            "    </div>\n" +
            "  </div>\n" +
            "</div>",
            modal = $(html);
        $('body').append(html);
        modal.find("form.ajax-form").on('submit', function (e) {
            var form = this;
            e.preventDefault();
            modal.on('hidden.bs.modal', function () {
                listing.modalForm(form);
            }).modal('hide');
        });
        modal.modal('show');
        modal.on('hidden.bs.modal', function () {
            modal.remove();
        });
    },

    mobilecheck: function () {
        var check = false;
        (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);

        return check;
    },

    filter: function (event, params) {
        var dynamicContentContainer = $('#dynamicContent');

        if (typeof event !== 'undefined')
            dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]');

        params = params || {};
        params.listingCommand = 'filter';
        var url = getUrlForRequest(event);

        var isMobile = listing.mobilecheck();

        if (isMobile) {
            var filterFields = listing.getFilterFields(event);
            // getFilterFields - returns data two times
            var halfOfFilterFields = filterFields.length / 2;
            var iterationCounter = 0;

            listing.getFilterFields(event).each(function () {

                if ((this.type !== "checkbox" && this.type !== "radio") || this.checked) {
                    params[this.name] = this.value;
                }

                if (iterationCounter === halfOfFilterFields - 1) {
                    return false;
                }

                iterationCounter++;
            });
        } else {
            listing.getFilterFields(event).each(function () {
                if ((this.type !== "checkbox" && this.type !== "radio") || this.checked) {
                    params[this.name] = this.value;
                }
            });
        }


        $(document).trigger("listingBeforeRequest");
        listing.sendRequest(params, url, dynamicContentContainer);
    },

    reset: function (event) {
        listing.getFilterFields(event).each(function () {
            $(this).val('');
        });
        listing.filter(event);
    },

    order: function (event) {
        event.preventDefault();
        var dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]');
        var _this = $(this);
        var params = {
            listingCommand: 'order',
            orderBy: _this.attr('orderBy'),
            orderType: _this.attr('orderType')
        };
        var url = getUrlForRequest(event);
        listing.sendRequest(params, url, dynamicContentContainer);
    },

    page: function (e) {
        var dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]');
        e.preventDefault();
        $modalParent = $(event.target).parents('.modal');
        if ($modalParent.length == 0)
            history.pushState({}, '', event.target.href);
        listing.sendRequest({}, event.target.href, dynamicContentContainer);
    },

    limit: function (event) {
        var $select = $(this);
        var $selected = $select.find('option[selected]');
        var limit = $select.val();
        var url = $selected.data('href').split('/').slice(0, -2).join('/');
        var params = {
            listingCommand: 'limit',
            limit: limit
        };
        var dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]');
        listing.sendRequest(params, url, dynamicContentContainer);
    },

    refresh: function (event) {
        if (typeof event !== 'undefined') {
            var dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]');
        }
        var url = getUrlForRequest(event);
        listing.sendRequest({}, url, dynamicContentContainer);
    },

    sendRequest: function (params, url, dynamicContainer) {
        var xhrArgs = {
            url: url,
            content: params,
            preventCache: true,
            handleAs: 'text',
            load: function (data) {
                listing.processResponse(data, dynamicContainer);
            },
            error: function (error) {
            }
        };
        throbeProcess.xhrPost(xhrArgs);

    },

    processResponse: function (data, dynamicContainer) {
        var dynamicContent = typeof dynamicContainer !== 'undefined' ? dynamicContainer.get(0) : false;

        if (!dynamicContent) {
            var dynamicModal = document.getElementById('dynamicContent_modal');
            if (dynamicModal) {
                var dynamicModalHTML = dynamicModal.innerHTML.replace(/^\s+/, '');

                dynamicContent = (dynamicModalHTML != '')
                    ? dynamicModal
                    : document.getElementById('dynamicContent');
            }
            else {
                dynamicContent = document.getElementById('dynamicContent');
            }
        }

        dynamicContent.innerHTML = data;

        listing.setup();

        if (dynamicContent.id.indexOf('dynamicContent_modal') >= 0) {
            var url = $(dynamicContent).parents('.modal').data('url');
            if (url.indexOf('user/select') !== -1) {
                setTimeout(function () {
                    document.body.classList.add('modal-open');
                }, 600);
            }
        }
    },

    onClickCollapsable: function (e) {
        if ($(e.target).closest('a').length) {
            return;
        }

        const $el = $(e.currentTarget).next('.collapsable');

        if ($el.is(':visible')) {
            $el.hide(100);
        } else {
            $('.collapsable').hide();
            $el.show(100);
        }
    }
};

function getUrlForRequest(event) {
    var url;
    if (typeof event !== 'undefined' && event.target.tagName === 'A' && event.target.href !== '') {
        url = event.target.href.split('/').slice(0, -2).join('/');
    }
    else {
        url = getModalUrl(event);
    }

    return url;
}

function getModalUrl(event) {
    var url = window.location.href;
    if (typeof event !== 'undefined') {
        var dynamicContentContainer = $(event.target).parents('[id^="dynamicContent"]'),
            $dcM_modal = dynamicContentContainer.parents('.modal');
        if ($dcM_modal.hasClass('in')) {
            url = $dcM_modal.data('url');
        }
    }
    return url;
}

document.addEventListener('load', listing.setup);