﻿$(function () {
    function dropdownMenuVisibilityMaster(dropdown) {
        var menu;

        $(dropdown).on('show.bs.dropdown', function () {
            var cordinates = $(this).offset();
            menu = $(this).find('.dropdown-menu');
            var height = $(this).height();
            $('body').append(menu.detach());
            menu.css({
                'top': cordinates.top + height + 'px',
                'left': cordinates.left + 'px',
                'display': 'block'
            });
        });

        $(dropdown).on('hide.bs.dropdown', function (e) {
            $(e.target).append(menu.detach());
            menu.hide();
        });
    }

    dropdownMenuVisibilityMaster($('td > div.dropdown'));

    $(document).on('listingSetup', function () {

        dropdownMenuVisibilityMaster($('td > div.dropdown'));
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('click', '.custom-dropdown', function () {
        $('#columnsControls').toggle();
    });
});