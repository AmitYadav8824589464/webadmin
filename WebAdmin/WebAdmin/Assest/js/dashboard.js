﻿function Dashboard(options) {
    var $elem = $(options.elem),
        refreshes = {},
        dashboardOptions = options.options,
        widgets = [],
        widgetArgs = {},
        editMode = false;

    (function () {
        setActionListeners();
    })();

    function setActionListeners() {
        $elem.on('click', '#btn_edit_mode', function (e) {
            editMode = !editMode;
            $elem.trigger('editMode', [editMode]);

            if (editMode) {
                $elem.find('#btn_add_block').fadeIn(200);
            } else {
                $elem.find('#btn_add_block').fadeOut(200);
            }
        });

        $elem.on('click', '#btn_reset_dashboard', function (e) {
            e.preventDefault();
            resetToDefault();
        });

        $elem.on('click', '#btn_add_block', function (e) {
            e.preventDefault();
            $.post(
                'dashboard/add-block',
                { dashboardName: dashboardOptions.name })
                .done(function (response) {
                    if (response.success) {
                        $elem.find('.blocks-wrapper').append(response.tpl);
                    }
                });
        });

        $elem.on('click', '.dashboard-options-btn', function (e) {
            $(e.target).parent().find('.dashboard-options-btn').each(function (i, el) {
                $(el).removeClass('active');
            });
            $(e.target).addClass('active');
            var optionName = Object.keys($(e.target).data())[0];
            var optionValue = $(e.target).data(optionName);
            dashboardOptions[optionName] = optionValue;
        });

        $elem.on('click', '.save-options-changes', function (e) {
            sendStateToServer();
        });
    }

    function sendStateToServer() {
        processLoad(true);
        $.post(
            'dashboard/update',
            { options: dashboardOptions, type: 'dashboard' }
        ).done(function (res) {
            if (res.success) {
                $elem.find('.alert-success').fadeIn(500);
                setTimeout(function () {
                    $elem.find('.alert-success').fadeOut(500);
                }, 3000);
            } else {
                $elem.find('.alert-error').fadeIn(500);
                $elem.find('.alert-error h4 span').html(res.errors);
                setTimeout(function () {
                    $elem.find('.alert-error').fadeOut(500);
                }, 3000);
            }

            processLoad(false);
        });
    }

    function processLoad(load) {
        var button = $elem.find('.save-options-changes');
        if (load) {
            button.attr('disabled', true);
            button.find('i').removeClass('hidden');
            button.find('span').addClass('hidden');
        } else {
            button.attr('disabled', false);
            button.find('i').addClass('hidden');
            button.find('span').removeClass('hidden');
        }
    }

    function addRefreshTask(time, name) {
        removeRefreshTask(name);

        var isFirstInInterval = !refreshes[time];

        if (refreshes.hasOwnProperty(time)) {
            refreshes[time].push(name);
        } else {
            refreshes[time] = [name];
        }
        if (isFirstInInterval) {
            runRefreshTasks(time);
        }
    }

    function removeRefreshTask(name) {
        var intervals = [];
        for (var interval in refreshes) {
            if (refreshes.hasOwnProperty(interval)) {
                if (refreshes[interval].indexOf(name) !== -1) {
                    refreshes[interval].splice(refreshes[interval].indexOf(name), 1);
                    intervals.push(interval);
                }
                if (refreshes[interval].length < 1) {
                    delete refreshes[interval];
                }
            }
        }
        return intervals;
    }

    function refreshWidgetDataImmediately(name, args) {
        var intervals = removeRefreshTask(name), rArgs = {};
        if (refreshes.hasOwnProperty(0))
            refreshes[0].push(name);
        else
            refreshes[0] = [name];
        if (args) {
            rArgs[name] = args;
            widgetArgs[name] = args;
        } else if (widgetArgs.hasOwnProperty(name))
            delete widgetArgs[name];
        refreshWidgetsData(0, rArgs);
        for (var i = 0; i < intervals.length; ++i) {
            if (refreshes.hasOwnProperty(intervals[i]))
                refreshes[intervals[i]].push(name);
            else
                refreshes[intervals[i]] = [name];
        }
        delete refreshes[0];
    }

    function refreshWidgetsData(interval, args) {
        if (refreshes[interval]) {
            for (var i = 0; i < refreshes[interval].length; ++i) {
                var widgetName = refreshes[interval][i], widget;
                if ((widget = getWidget(widgetName.replace('-', '_')))) {
                    getWidget(widgetName).getElem().trigger('refreshWidgetDataBefore');
                }
            }
            $.get(
                'dashboard/get-data',
                { widgets: refreshes[interval], dashboard: dashboardOptions.name, args: args }
            ).done(function (response) {
                if (response.success) {
                    for (var widgetName in response.data) {
                        if (response.data.hasOwnProperty(widgetName)) {
                            var newData = response.data[widgetName], widget;
                            widgetName = widgetName.replace('-', '_');
                            widget = getWidget(widgetName);
                            widget.refreshData(newData);
                            widget.getElem().trigger('refreshWidgetDataAfter');
                        }
                    }
                }
                if (interval > 0)
                    setTimeout(function () {
                        refreshWidgetsData(interval, args)
                    }, interval);
            });
        }
    }

    function runRefreshTasks(interval) {
        if (refreshes.hasOwnProperty(interval)) {
            refreshWidgetsData(interval, widgetArgs);
        }
    }

    function getName() {
        return dashboardOptions['name'];
    }

    function addWidget(data) {
        var widget, parts, i, widgetName;
        switch (data.options.mode) {
            case 'text':
                widget = new WidgetText(data);
                break;
            case 'chart':
                widget = new WidgetChart(data);
                break;
            case 'table':
                widget = new WidgetTable(data, this);
                break;
            default:
                parts = data.options.mode.split('-');
                for (i = 0; i < parts.length; ++i)
                    parts[i] = parts[i].charAt(0).toUpperCase() + parts[i].slice(1);
                widgetName = 'Widget' + parts.join('');
                if (window.hasOwnProperty(widgetName))
                    widget = new window[widgetName](data, this);
                break;
        }
        widgets[data.options.name] = widget;
    }

    function removeWidget(widgetName) {
        $('#' + widgetName).remove();
        delete widgets[widgetName];
    }

    function getWidget(widgetName) {
        return widgets[widgetName];
    }

    function getElem() {
        return $elem;
    }

    function isEditMode() {
        return editMode;
    }

    function resetToDefault() {
        var c = confirm('Your current dashboard layout will be lost. Are you sure?');
        if (!c) return;

        processLoad(true);
        $.post(
            'dashboard/reset-dashboard',
            { name: dashboardOptions.name }
        ).done(function (response) {
            if (response.success) {
                window.location.reload();
            }
            processLoad(false);
        });
    }

    this.addRefreshTask = addRefreshTask;
    this.removeRefreshTask = removeRefreshTask;
    this.getName = getName;
    this.addWidget = addWidget;
    this.removeWidget = removeWidget;
    this.getWidget = getWidget;
    this.getElem = getElem;
    this.isEditMode = isEditMode;
    this.refreshWidgetDataImmediately = refreshWidgetDataImmediately;
}