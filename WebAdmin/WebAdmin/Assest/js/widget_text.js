﻿function WidgetText(options) {
    var $elem = options.elem ? $(options.elem) : null,
        widgetOptions = options.options,
        initText = '';

    (function init() {
        Widget.init(widgetOptions, $elem);
        initText = $elem.find('.data-wrapper').text().trim();
        setTimeout(function () {
            resizeDataText();
        }, 200);
    })();

    function getElem() {
        if (!$elem) render();
        return $elem;
    }

    function render(html) {
        $elem = html;
        setSettingsListeners();
    }

    function setSettingsListeners() {
        $(window).resize(function () {
            resizeDataText();
        });

        $elem.on('click', '.widget-settings', function (e) {
            Widget.settingsMenu.watchDirectionDropdown($elem);
        });

        $elem.on('click', '.dropdown-submenu', function (e) {
            e.preventDefault();
        });

        $elem.on('click', '.widget-update-link', function (e) {
            e.preventDefault();
            var optionName = Object.keys($(e.target).data())[0];
            var optionValue = $(e.target).data(optionName);

            Widget.setState(widgetOptions['name'], optionName, optionValue);

            if (optionName === 'color') {
                $elem.find('.small-box').attr('style', 'background-color: #' + optionValue + ' !important');
            }
        });
    }

    function refreshData(value) {
        $elem.find('.data-wrapper').text(value);
        Widget.setTooltips(widgetOptions, value);
    }

    if ($elem) {
        setSettingsListeners();
    }

    this.getOptions = function () {
        return widgetOptions;
    };

    function resizeDataText() {
        var width = $elem.width();
        var $dataWrapper = $elem.find('.data-wrapper');
        var maxChars = 10;

        if ($dataWrapper.width() > $dataWrapper.parent().width()) {
            $dataWrapper.addClass('small-data-text');
            var end = (initText.length - maxChars) * -1;
            $dataWrapper.text(initText.slice(0, end) + '...');
        } else if (width / 2 > $dataWrapper.width()) {
            $dataWrapper.removeClass('small-data-text');
            $dataWrapper.text(initText);
        }
    }

    this.getElem = getElem;
    this.refreshData = refreshData;
}