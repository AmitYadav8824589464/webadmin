﻿function widgetInit() {
    var tippyList = [];

    return {

        init: function (widgetOptions, $elem) {
            if (widgetOptions.interval && parseInt(widgetOptions.interval) !== 0) {
                dashboard.addRefreshTask(widgetOptions.interval, widgetOptions.name);
            }
            this.settingsMenu.editMode($elem, dashboard.isEditMode());
            dashboard.getElem().on('editMode', function (e, editMode) {
                Widget.settingsMenu.editMode($elem, editMode);
            });
            this.setTooltips(widgetOptions, $elem.find('.data-wrapper').text().trim());
        },

        setState: function (widgetName, stateName, stateValue, sendStateToServer = true) {
            var widgetOptions = dashboard.getWidget(widgetName).getOptions();
            if (!widgetOptions[stateName] && widgetOptions[stateName] !== 0) return;

            this.settingsMenu.changeItemIcon(widgetName, stateName, stateValue);
            widgetOptions[stateName] = stateValue;
            var loadData = true;

            if (stateName === 'interval') {
                loadData = false;

                if (stateValue === 0) {
                    dashboard.removeRefreshTask(widgetOptions.name);
                } else {
                    // cache icon ------
                    var cachedIcon = $('.widget[id=' + widgetName + ']').find('.cached');
                    if (cachedIcon.length) {
                        cachedIcon.fadeOut(1000);
                    }
                    this.settingsMenu.changeItemIcon(widgetName, 'cache', 0);
                    //-----------------

                    dashboard.addRefreshTask(stateValue, widgetOptions.name);
                }
            }

            if (sendStateToServer) {
                this.sendStateToServer(widgetOptions, loadData);
            }
        },

        settingsMenu: {

            changeItemIcon: function (widgetName, optionName, optionValue) {
                var elem = dashboard.getWidget(widgetName).getElem();
                elem.find('.widget-update-' + optionName + ' .fa-check')
                    .removeClass('hidden')
                    .addClass('hidden');
                elem.find('.widget-update-' + optionName + '[data-' + optionName + '=' + optionValue + ']')
                    .find('.fa-check')
                    .removeClass('hidden');
            },

            watchDirectionDropdown: function (elem) {
                var position = $(elem).position();
                var windowWidth = $(document).find('.content-wrapper').width();
                var widgetWidth = $(elem).width();
                var menuWidth = $(elem).find('.dropdown-menu').width() * 2;

                if (windowWidth - (position.left + widgetWidth) > menuWidth) {
                    $(elem).find('.widget-settings > ul').addClass('menu-right');
                } else {
                    $(elem).find('.widget-settings > ul').removeClass('menu-right');
                }
            },

            processLoad: function (widgetName, load) {
                if (!dashboard.getWidget(widgetName)) return;

                var elem = dashboard.getWidget(widgetName).getElem();
                var button = elem.find('.widget-text-settings-button');
                var loader = elem.find('.loader-bg:first');

                if (load) {
                    button.find('.fa-gear').addClass('fa-spin');
                    button.addClass('disabled');
                    $(loader).removeClass('hidden');
                } else {
                    button.find('.fa-gear').removeClass('fa-spin');
                    button.removeClass('disabled');
                    $(loader).addClass('hidden');
                }
            },

            editMode: function ($elem, editMode) {
                if (editMode) {
                    $elem.find('.widget-settings').fadeIn(200);
                    $elem.addClass('edited');
                } else {
                    $elem.find('.widget-settings').fadeOut(200);
                    $elem.removeClass('edited');
                }
            }
        },

        sendStateToServer: function (widgetOptions, loadData) {
            var data = {
                type: 'widget',
                options: widgetOptions
            };

            var self = this;
            this.settingsMenu.processLoad(widgetOptions['name'], true);

            $.post(
                'dashboard/update',
                data
            ).done(function (response) {
                if (response.success) {
                    if (loadData) {
                        self.getData([widgetOptions['name']]);
                    }
                }
                self.settingsMenu.processLoad(widgetOptions['name'], false);
            });
        },

        remove: function (blockName, widgetName) {
            var settingsMenu = this.settingsMenu;
            settingsMenu.processLoad(widgetName, true);

            $.post(
                'dashboard/delete',
                { name: widgetName, block: blockName }
            ).done(function (response) {
                if (response.success) {
                    dashboard.removeWidget(widgetName);
                    settingsMenu.processLoad(widgetName, false);
                }
            });
        },

        getData: function (widgetNames) {
            var settingsMenu = this.settingsMenu;

            widgetNames.forEach(function (name) {
                settingsMenu.processLoad(name, true);
            });

            $.get(
                'dashboard/get-data',
                { widgets: widgetNames, dashboard: dashboard.getName() }
            ).done(function (response) {
                if (response.success) {
                    for (var widgetName in response.data) {
                        if (response.data.hasOwnProperty(widgetName)) {
                            var widgetData = response.data[widgetName];
                            var widget = dashboard.getWidget(widgetName);
                            widget.refreshData(widgetData);
                            settingsMenu.processLoad(widgetName, false);
                        }
                    }
                }
            });
        },

        getOptions: function (widgetName) {
            var widget = dashboard.getWidget(widgetName);
            return widget.getOptions();
        },

        setTooltips: function (widgetOptions, data) {
            setTimeout(function () {
                if (widgetOptions.hasOwnProperty('description') || widgetOptions.mode === 'text') {
                    var text = widgetOptions.hasOwnProperty('description') ? widgetOptions.description : '';
                    if (widgetOptions.mode === 'text') {
                        text += widgetOptions.hasOwnProperty('description') ? '<br>' + data : data;
                    }

                    if (!tippyList[widgetOptions.name]) {
                        tippyList[widgetOptions.name] = tippy('#' + widgetOptions.name, {
                            content: text,
                            size: 'large',
                            theme: 'max',
                            animation: 'scale'
                        });
                    } else {
                        tippyList[widgetOptions.name][0].setContent(text);
                    }

                }
            }, 200);
        }
    };
}

var Widget = widgetInit();