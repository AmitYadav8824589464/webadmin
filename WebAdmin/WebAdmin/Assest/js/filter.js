﻿var tables = {};

tables.table = {};

tables.loadColumnsData = function () {
    var tableId = $('table').attr('id');
    if (tableId === undefined) {
        return;
    }
    tables.table[tableId] = {};
    tables.table[tableId].headerInfo = tables.getHeaderInfo(tableId);
    tables.table[tableId].columnVisibility = tables.loadColumnVisibility(tableId);
    var headerInfo = tables.table[tableId].headerInfo;
    var columnVisibility = tables.table[tableId].columnVisibility;

    $('#columnsControls').html('');

    $.each(headerInfo.list, function (index, columnId) {
        var column = headerInfo.data[columnId];
        var checked = columnVisibility[columnId] ? 'checked' : '';
        var html = "<div class='manage-control-checkbox'>" +
            "<label>" +
            "<input name='manage-" + columnId + "' type='checkbox' " + checked + " id='manage-control-checkbox-" + columnId + "' data-column-id='" + column.id + "'/>" +
            "<label style='margin-left: 5px'>" + column.title + "</label>" +
            "</label>" +
            "</div>";

        $('#columnsControls').append(html);
    });

    // Init iCheck chekboxes
    $('#columnsControls input').each(function () {
        var self = $(this);

        self.iCheck({
            checkboxClass: 'icheckbox_minimal-purple',
            radioClass: 'iradio_minimal-purple',
        });
    });
};


tables.getHeaderInfo = function (tableId) {
    if (tableId == undefined) {
        return;
    }

    var info = {
        list: [],
        data: {}
    };

    var $thisTable = $('#' + tableId);
    var i = -1;
    $thisTable.find('thead:not(.listFilter) th').each(function (index) {
        i++;
        var $thisTh = $(this);
        if (!$thisTh.attr('data-meta-key')) {
            return null;
        }
        var item = {
            id: $thisTh.attr('data-meta-key'),
            title: $.trim($thisTh.text()),
            index: i
        };

        info.data[item.id] = item;
        info.list.push(item.id);
    });

    return info;
};


tables.loadColumnVisibility = function (tableId) {
    var storageKey = 'column-visibility-' + tableId + '-' + window.location.pathname,
        headerInfo = tables.table[tableId].headerInfo,
        storageValue = {},
        columnVisibility = {};

    storageValue = JSON.parse(localStorage.getItem(storageKey));

    $.each(headerInfo.list, function (index, columnId) {
        columnVisibility[columnId] = !(storageValue && (columnId in storageValue) && !storageValue[columnId]);
    });

    return columnVisibility;
};


tables.saveColumnVisibility = function (tableId, columnVisibility) {
    var storageKey = 'column-visibility-' + tableId + '-' + window.location.pathname;

    localStorage.setItem(storageKey, JSON.stringify(columnVisibility));
};


tables.manageColumns = function (table) {
    var tableId = table.tables().nodes().to$().attr('id');

    if (!(tableId in tables.table)) {
        return;
    }

    var headerInfo = tables.table[tableId].headerInfo;
    var columnVisibility = tables.loadColumnVisibility(tableId);

    $.each(headerInfo.list, function (index, columnId) {
        var columnIndex = headerInfo.data[columnId].index;
        var column = table.column(columnIndex);
        column.visible(columnVisibility[columnId]);
    });

    $('#columnsControls input:checkbox').on('ifChanged', function (event) {
        var $this = $(this);
        var headerInfo = tables.table[tableId].headerInfo;
        var columnId = $this.attr('data-column-id');
        var columnIndex = headerInfo.data[columnId].index;
        var column = table.column(columnIndex);

        column.visible(!column.visible());

        var columnVisibility = tables.loadColumnVisibility(tableId);
        columnVisibility[columnId] = !columnVisibility[columnId];
        tables.saveColumnVisibility(tableId, columnVisibility);
    });
};
var _initTable;
tables.buildTable = function (_table) {

    var table_id = _table.id.replace(/(!|"|#|\$|%|\'|\(|\)|\*|\+|\,|\.|\/|\:|\;|\?|@)/g, function ($1, $2) {
        return "\\" + $2;
    });

    function getScrollYHeight() {

        var windowHeight, containerOffsetTop;
        var rowCountHeight;
        var resultHeight;
        var emptySpace = 10;

        if (!$('#dynamicContent').length) {
            return '60vh';
        }

        windowHeight = $(window).height();
        containerOffsetTop = $('#dynamicContent').offset().top;

        rowCountHeight = $('.rowCount').height();
        rowCountHeight = Math.floor(rowCountHeight / windowHeight * 100) || 0;

        if (containerOffsetTop >= windowHeight / 2) {
            resultHeight = 100;
        } else {
            resultHeight = 100 - Math.floor(containerOffsetTop / windowHeight * 100);
        }
        resultHeight = resultHeight - rowCountHeight - emptySpace;

        return resultHeight + 'vh';
    }

    var notHasFixedCols = $(_table).data('dt-not-fixed-cols');
    var columnDefs = [];
    var countFixedLeftCols = 0;
    var columns = $(_table).find('.listHead tr:first-child th');
    columns.each(function (i, el) {
        var width = $(el).data('dt-width');
        if (typeof width !== 'undefined') {
            columnDefs.push({
                width: width,
                targets: i
            })
        }
        if ($(el).data('dt-fixed-left')) countFixedLeftCols++;
    });

    // if (columnDefs.length === 0) {
    //     columnDefs.push({
    //         width: 60,
    //         targets: 0
    //     });
    // }

    var fixedColumns = {
        leftColumns: countFixedLeftCols > 0 ? countFixedLeftCols : 1,
        fnDrawCallback: function (left) {
            $([left.body, left.header]).find('input').iCheck({
                checkboxClass: 'icheckbox_minimal-purple',
                radioClass: 'icheckbox_minimal-purple'
            });
        }
    };

    if (notHasFixedCols) {
        fixedColumns = null;
    }

    var defaultParams = {
        "autoWidth": true,
        "ordering": false,
        "stateSave": true,
        "bSortCellsTop": true,
        "lengthChange": true,
        "searching": false,
        "scrollY": getScrollYHeight(),
        "scrollCollapse": true,
        'scrollX': true,
        "paging": false,
        "fixedColumns": fixedColumns,
        "columnDefs": columnDefs,
        "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "initComplete": function (settings, json) {
            listing.setup();
        },
        'drawCallback': function (settings) {
            this.find('.listFilter').remove();
        }
    };

    _initTable = $(_table).DataTable(defaultParams);

    $('#' + table_id + '_wrapper' + ' .dataTables_scrollFoot').insertBefore('#' + table_id + '_wrapper' + ' .dataTables_scrollBody');

    tables.manageColumns(_initTable);
};

tables.initBulkActionControls = function () {
    $('a.bulk-action').on('click', function (e) {
        e.preventDefault();
        var a = $(this), items = [], key, params = {};
        if (undefined !== a.data('confirm') && !confirm(a.data('confirm'))) {
            return false;
        }
        $('.rowCheckbox:checked:not(#rowCheckboxAll)').each(function () {
            if (-1 === items.indexOf(this.value))
                items.push(this.value)
        });
        if (items.length === 0)
            return false;
        key = a.data('key') || 'ids';
        params[key] = items;
        throbeProcess.xhrPost({
            url: a.attr('href'),
            content: params,
            preventCache: true,
            handleAs: 'json',
            load: function (data) {
                if (!data.success)
                    alert(data.errors.join(";\n") + '.');
                setTimeout(listing.refresh, 650); // as underlay timeout in throbeProcess.hideThrobber
            }
        });

        return false;
    });
};

tables.initTable = function () {
    var _tables = document.querySelectorAll('.listing-table');
    _tables.forEach(function (_table) {
        if (!$(_table).hasClass('dataTable'))
            tables.buildTable(_table);
    });
};

tables.reinit = function (callback) {
    _initTable.destroy();
    if (callback) {
        callback();
    }
    tables.loadColumnsData();
    tables.initTable();
};

tables.initColumnsControls = function () {

    function dropdownMenuVisibilityMaster(dropdown) {
        var menu;

        $(dropdown).on('show.bs.dropdown', function () {
            var cordinates = $(this).offset();
            menu = $(this).find('.dropdown-menu');
            var height = $(this).height();
            $('body').append(menu.detach());
            menu.css({
                'top': cordinates.top + height + 'px',
                'left': cordinates.left + 'px',
                'display': 'block'
            });
        });

        $(dropdown).on('hide.bs.dropdown', function (e) {
            $(e.target).append(menu.detach());
            menu.hide();
        });
    }

    dropdownMenuVisibilityMaster($('td > div.dropdown'));

    $(document).on('listingSetup', function () {

        dropdownMenuVisibilityMaster($('td > div.dropdown'));
    });

    $('[data-toggle="tooltip"]').tooltip();

    return {
        init: function () {
            $('documnet').off('click').on('click', '.custom-dropdown', function (event) {
                console.log('hey');
                $('#columnsControls').toggle();
            });

            $(document).on('click', function (event) {
                console.log(event);
                var $menu = $('#columnsControls');
                if (!$(event.target).closest('#columnControlsBtn').length && !$(event.target).closest('#columnsControls').length && $menu.is(':visible')) {
                    $menu.hide();
                }
            });
        }
    };
};


tables.init = function () {
    tables.loadColumnsData();
    tables.initTable();
    tables.initBulkActionControls();
    $(document).on('listingSetup', function (event) {
        tables.loadColumnsData();
        tables.initTable();
    });
    $(document).on('listingBeforeRequest', function (event) {
        _initTable && _initTable.destroy();
    });
};

$(document).ready(function () {
    tables.init();

    $(document).on('click', function (event) {
        var $menu = $('#columnsControls');
        if (!$(event.target).closest('#columnControlsBtn').length && !$(event.target).closest('#columnsControls').length && $menu.is(':visible')) {
            $menu.hide();
        }
    });
});