﻿var throbeProcess = {
    hideUnderlay: false,
    processes: [],
    throbberContainerId: '#throberContainer',
    throbberErrorContainerId: '#throbberErrorContainer',
    processCounter: 0,
    addProcess: function (processId) {
        if (typeof processId == 'number' || typeof processId == 'string') {
            this.processes[this.processes.length] = processId;
        }
        this.showThrobber();
    },
    removeProcess: function (processId) {
        for (i in this.processes) {
            if (this.processes[i]) {
                this.processes.splice(i, 1);
                break;
            }
        }
        if (this.processes.length === 0) {
            this.hideThrobber();
        }
    },
    showThrobber: function () {
        $(throbeProcess.throbberContainerId).modal('show');
    },
    hideThrobber: function () {
        setTimeout(function () {
            $(throbeProcess.throbberContainerId).modal('hide')
        }, 300);
    },
    isProcessExists: function () {
        return this.processes.length != 0;
    },
    _generateRequestId: function () {
        return ++this.processCounter;
    },
    _prepareXhrArgsAndLanchProces: function (args) {
        var processId = this._generateRequestId();
        var load = function () { };
        var error = function (data) {
            var modal = $(throbeProcess.throbberErrorContainerId);
            modal.find('.modal-content').html(data.responseText);
            modal.modal('show');
        };
        var _error = function () { };
        if (typeof args.load == 'function') {
            load = args.load;
        }
        if (typeof args.error == 'function') {
            _error = error;
            error = args.error;
        }
        args.load = function (data) {
            if ((typeof args.handleAs == 'string') && (args.handleAs == 'text') && data.match(/^\{.*\}$/)) {
                window.location = window.location.href;
                return;
            }
            load(data);
            throbeProcess.removeProcess(processId);
        };
        args.error = function (data) {
            throbeProcess.removeProcess(processId);
            error(data, _error);
        };
        this.addProcess(processId);
        return args;
    },
    xhrPost: function (args) {
        var postArgs = this._prepareXhrArgsAndLanchProces(args);

        var data;
        if (postArgs.content) {
            data = postArgs.content;
        }
        if (postArgs.form) {
            var form = typeof postArgs.form == 'string' ? $('#' + postArgs.form) : postArgs.form;
            data = form.serialize();
            if (!postArgs.url) {
                postArgs.url = form.attr('action');
            }
        }
        if (postArgs.content && postArgs.form) {
            data = $('#' + postArgs.form).serialize();
            $.each(postArgs.content, function (index, value) {
                data += '&' + index + "=" + value;
            });
        }

        $.post(postArgs.url, data, postArgs.load, postArgs.handleAs).fail(postArgs.error);
    },
    xhrGet: function (args) {
        var getArgs = this._prepareXhrArgsAndLanchProces(args);
        $.get(getArgs.url, getArgs.load, getArgs.handleAs).fail(getArgs.error);
    }
};