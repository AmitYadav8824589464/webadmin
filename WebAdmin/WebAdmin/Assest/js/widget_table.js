﻿function WidgetTable(options, dashboard) {
    var $elem = options.elem ? $(options.elem) : null,
        widgetOptions = options.options;

    (function init() {
        Widget.init(widgetOptions, $elem);
        if ($elem) {
            setSettingsListeners();
        }
    })();

    function getElem() {
        if (!$elem) render();
        return $elem;
    }

    function render(html) {
        $elem = html;
        setSettingsListeners();
    }

    function setSettingsListeners() {
        $elem.on('click', '.widget-settings', function (e) {
            Widget.settingsMenu.watchDirectionDropdown($elem);
        });

        $elem.on('click', '.dropdown-submenu', function (e) {
            e.preventDefault();
        });

        $elem.on('click', '.widget-update-link', function (e) {
            e.preventDefault();
            var optionName = Object.keys($(e.target).data())[0];
            var optionValue = $(e.target).data(optionName);
            Widget.setState(widgetOptions['name'], optionName, optionValue);
        });
    }

    function setPage(e) {
        var a = $(e.target);
        dashboard.refreshWidgetDataImmediately(widgetOptions.name, a.data());
        return false;
    }

    function refreshData(data) {
        var container = $elem.find('.data-container'), footer = $elem.find('.box-footer'), meta = {};
        data = typeof data == 'string' ? JSON.parse(data) : data;
        if (data.hasOwnProperty('items')) {
            if (data.hasOwnProperty('meta'))
                meta = data.meta;
            data = data.items;
        }
        if (data && data.length) {
            $elem.find('.no-data').addClass('hidden');
            $elem.find('table').removeClass('hidden');
            var table = '', idD = 1;
            container.html('');
            if (meta.hasOwnProperty('pages')) {
                idD = (meta.pages.current - 1) * meta.pages.perPage + 1;
            }
            data.forEach(function (item, i) {
                i = i + idD;
                var colNames = Object.keys(item);
                var row = '<tr>';
                row += '<td>' + i + '</td>';
                colNames.forEach(function (name) {
                    row += '<td>' + item[name] + '</td>';
                });
                row += '</tr>';
                table += row;
            });
            container.append(table);
            processMeta(meta);
        } else {
            container.html('');
            $elem.find('.no-data').removeClass('hidden');
            $elem.find('table').addClass('hidden');
            footer.addClass('hidden');
        }
    }

    function processMeta(meta) {
        var footer = $elem.find('.box-footer'), paginator, i, pPage, pPageA, numPrevLinks, numNextLinks;
        if (meta.hasOwnProperty('pages')) {
            footer.removeClass('hidden');
            paginator = footer.find('ul.pagination');
            paginator.removeClass('hidden');
            paginator.find('li:not(.previous):not(.next)').remove();
            var pFirst = paginator.find('.previous');
            var pLast = paginator.find('.next');
            if (meta.pages.current > 1)
                pFirst.removeClass('disabled');
            else
                pFirst.addClass('disabled');
            pFirst.find('a').data('page', 1);
            if (meta.pages.current < meta.pages.total)
                pLast.removeClass('disabled');
            else
                pLast.addClass('disabled');
            pLast.find('a').data('page', meta.pages.total);
            numPrevLinks = meta.pages.current - 2;
            numNextLinks = meta.pages.total - meta.pages.current;
            if (numNextLinks < 2) {
                if (numPrevLinks > 4 - numNextLinks)
                    numPrevLinks = 4 - numNextLinks;
                else if (numPrevLinks < 0)
                    numPrevLinks = 0;
            } else {
                if (numPrevLinks > 2)
                    numPrevLinks = 2;
                else if (numPrevLinks < 0)
                    numPrevLinks = 0;
            }
            for (i = 0; i < numPrevLinks; ++i) {
                pPage = $('<li>').addClass('paginate_button');
                pPageA = $('<a>').data('page', meta.pages.current - numPrevLinks + i).attr('href', '#').html(meta.pages.current - numPrevLinks + i).click(setPage);
                pPage.append(pPageA);
                pLast.before(pPage);
            }
            pPage = $('<li>').addClass('paginate_button').addClass('active');
            pPageA = $('<a>').data('page', meta.pages.current).attr('href', '#').html(meta.pages.current).click(function () {
                return false;
            });
            pPage.append(pPageA);
            pLast.before(pPage);
            if (numNextLinks > 4 - numPrevLinks)
                numNextLinks = 4 - numPrevLinks;
            for (i = 0; i < numNextLinks; ++i) {
                pPage = $('<li>').addClass('paginate_button');
                pPageA = $('<a>').data('page', meta.pages.current + i + 1).attr('href', '#').html(meta.pages.current + i + 1).click(setPage);
                pPage.append(pPageA);
                pLast.before(pPage);
            }
        } else {
            footer.find('ul.pagination').addClass('hidden');
        }
    }

    if ($elem) {
        $elem.on("refreshWidgetDataBefore", function () {
            $elem.find("li.paginate_button:not(.disabled):not(.active)").each(function () {
                $(this).addClass('disabled');
            });
        });
        $elem.find('.previous,.next').click(setPage);
    }
    if (options['data'] && options['data']['meta']) {
        processMeta(options['data']['meta']);
    }

    this.getOptions = function () {
        return widgetOptions;
    };

    this.getElem = getElem;
    this.refreshData = refreshData;
}