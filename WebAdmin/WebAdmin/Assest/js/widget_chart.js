﻿function WidgetChart(options) {
    var $elem = options.elem ? $(options.elem) : null,
        widgetOptions = options.options,
        widgetData = 'string' == typeof options.data ? JSON.parse(options.data) : options.data;
    var chart;
    var chartConfig;
    var colors = [
        {
            fill: '#F5CCE8',
            border: '#C880B7',
            hovFill: '#9F6BA0',
            hovBorder: '#4A2040'
        },
        {
            fill: '#CCDAD1',
            border: '#9CAEA9',
            hovFill: '#87D68D',
            hovBorder: '#87D68D'
        },
        {
            fill: '#BCC4DB',
            border: '#6BBAEC',
            hovFill: '#6987C9',
            hovBorder: '#7880B5'
        }
    ];

    (function init() {
        Widget.init(widgetOptions, $elem);
        chartConfig = initChartConfig(widgetData, widgetOptions.charttype);
        createChart();
    })();

    function initChartConfig(data, type) {
        var dateFormat, subt, unit, displayFormat, period;
        var fill = false;

        switch (parseInt(widgetOptions['period'])) {
            case 1:
                period = 24;
                dateFormat = 'YYYY-MM-DD HH';
                subt = 'hours';
                unit = 'hour';
                displayFormat = 'DD/HH';
                break;
            case 365:
                period = 12;
                dateFormat = 'YYYY-MM';
                subt = 'months';
                unit = 'month';
                displayFormat = 'MM/YY';
                break;
            default:
                period = parseInt(widgetOptions['period']);
                dateFormat = 'YYYY-MM-DD';
                subt = 'days';
                unit = 'day';
                displayFormat = 'DD/MM';
                break;
        }

        var datasets = [];
        var legend = data.data.length > 1;
        var formatToView = data.options.format || '%1';
        var precision = data.options.precision || 0;

        data.data.forEach(function (d, n) {
            var chartData = 'string' === typeof d.data ? JSON.parse(d.data) : d.data;
            var dataFull = [];
            var nowDate = moment();

            for (var i = 0; i < period; i++) {
                var setData = chartData.find(function (date) {
                    return date.t === nowDate.format(dateFormat);
                });

                if (!setData) setData = { t: nowDate.format(dateFormat), y: 0 };
                dataFull.push(setData);
                nowDate.subtract(1, subt);
            }

            dataFull = dataFull.reverse();

            var dataset = {
                label: d.label,
                data: dataFull,
                borderWidth: 0,
                borderColor: colors[n].border,
                backgroundColor: colors[n].fill,
                hoverBorderWidth: 0,
                hoverBorderColor: colors[n].hovBorder,
                hoverBackgroundColor: colors[n].hovFill,
                fill: fill
            };

            datasets.push(dataset);
        });


        return {
            type: type,
            data: {
                datasets: datasets
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: legend
                },
                title: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            parser: dateFormat,
                            tooltipFormat: parseInt(widgetOptions['period']) === 1 ? 'LT' : 'll',
                            unit: unit,
                            displayFormats: {
                                day: displayFormat
                            }
                        },
                        scaleLabel: {
                            display: false,
                            labelString: 'Date'
                        },
                        gridLines: {
                            display: false
                        },
                        stacked: true
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: false,
                            labelString: 'value'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            beginAtZero: true,
                            callback: function (value) {
                                if (value % 1 === 0) {
                                    return formatValue(value, formatToView, precision);
                                }
                            }
                        },
                        stacked: true
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label;
                            return label + ': ' + formatValue(tooltipItem.yLabel, formatToView, precision);
                        }
                    }
                }
            }
        }
    }

    function formatValue(num, format, precision) {
        return format.replace('%1', num.toFixed(precision).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    }

    function createChart() {
        var ctx = $elem.find('#chart');
        if (chart)
            chart.destroy();
        chart = new Chart(ctx, chartConfig);
    }

    function updateChart(data, type) {
        chartConfig = initChartConfig(data, type);
        createChart();
    }

    function getElem() {
        if (!$elem) render();
        return $elem;
    }

    function render(html) {
        $elem = html;
        setSettingsListeners();
    }

    function setSettingsListeners() {
        $elem.on('click', '.widget-settings', function (e) {
            Widget.settingsMenu.watchDirectionDropdown($elem);
        });

        $elem.on('click', '.dropdown-submenu', function (e) {
            e.preventDefault();
        });

        $elem.on('click', '.widget-update-link', function (e) {
            e.preventDefault();
            var optionName = Object.keys($(e.target).data())[0];
            var optionValue = $(e.target).data(optionName);
            Widget.setState(widgetOptions['name'], optionName, optionValue);
        });
    }

    function refreshData(data) {
        var d = typeof data == 'string' ? JSON.parse(data) : data;
        updateChart(d, widgetOptions['charttype']);
    }

    if ($elem) {
        setSettingsListeners();
    }

    this.getOptions = function () {
        return widgetOptions;
    };

    this.getElem = getElem;
    this.refreshData = refreshData;
}