﻿function Block(options) {
    var $elem = $(options.elem),
        blockOptions = options.options,
        $settings = $elem.find('.settings');

    (function init() {
        setSettingsListeners();
        editMode(dashboard.isEditMode());

        var drake = dragula([$elem.find('.widgets-container')[0]], {
            moves: function () {
                return dashboard.isEditMode();
            }
        });
        drake.on('drop', function () {
            updatePositions();
            sendStateToServer();
        })
    })();

    function setSettingsListeners() {
        dashboard.getElem().on('editMode', function (e, edit) {
            editMode(edit);
        });

        $elem.on('mouseenter', '.submenu-add-widget', function (e) {
            menuPosition();
        });

        $elem.on('click', '.settings a', function (e) {
            e.preventDefault();
        });

        // selection of existing widgets in the menu
        $elem.on('click', '.block-settings-button', function (e) {
            var dashboardWidgets = [];
            $('.widget').each(function (i, elem) {
                if ($(elem).data().name) {
                    dashboardWidgets.push($(elem).data().name);
                }
            });

            $(e.currentTarget).parent().find('.submenu-add-widget li a').each(function (i, elem) {
                var name = $(elem).data().type;

                if (dashboardWidgets.indexOf(name) + 1) {
                    $(elem).addClass('widget-isset');
                } else {
                    $(elem).removeClass('widget-isset');
                }
            });
        });

        $elem.on('click', '.delete-block', function (e) {
            e.preventDefault();

            $.post(
                'dashboard/delete-block',
                { blockName: blockOptions.name, dashboardName: dashboard.getName() })
                .done(function (response) {
                    if (response.success) {
                        // remove refresh task inner widgets
                        for (var name in blockOptions.positions) {
                            dashboard.removeRefreshTask(name);
                        }

                        $elem.remove();
                        delete window[blockOptions.name];
                    }
                });
        });
        $settings.on('click', '.new-widget', function (e) {
            e.preventDefault();
            processLoad(true);

            $.post(
                'dashboard/add-widget',
                { dashboardName: dashboard.getName(), blockName: blockOptions.name, widgetType: $(e.target).data('type') })
                .done(function (response) {
                    processLoad(false);
                    if (response.success) {
                        blockOptions.positions[response.data.options.name] = ($elem.find('.widget')).length + 1;
                        dashboard.addWidget(response.data);
                        $elem.find('.widgets-container').append(dashboard.getWidget(response.data.options.name).getElem());
                        initialOptionsForNewWidgets(response.data.options.name);
                        $elem.find('.empty-block').remove(); // remove empty block message
                    }
                });
        });

        $settings.on('click', '.block-update-link', function (e) {
            e.preventDefault();

            var name = Object.keys($(e.target).data())[0];
            var value = $(e.target).data(name);
            blockOptions[name] = value;
            $elem.find('.block-update-' + name + ' .fa-check')
                .removeClass('hidden')
                .addClass('hidden');
            $(e.target).find('.fa-check').removeClass('hidden');
            sendStateToServer();
            setWidgetsOption(name, value);
        });

        // Remove widget
        $elem.on('click', '.widget-remove', function (e) {
            e.preventDefault();
            var widgetName = $(e.target).attr('widget');
            Widget.remove(blockOptions.name, widgetName);
            dashboard.removeRefreshTask(widgetName);
        })
    }

    function setWidgetsOption(name, value) {
        var widgetsDataUpdate = [];
        var widgetNames = [];
        processLoad(true);

        $elem.find('.widget').each(function (i, el) {
            if ($(el).find('.dropdown-submenu').hasClass(name)) {
                var widgetName = $(el).attr('id');
                var options = Widget.getOptions(widgetName);
                if (options.hasOwnProperty(name)) {
                    options[name] = value;
                    Widget.setState(widgetName, name, value, false);
                    widgetsDataUpdate.push(options);
                    widgetNames.push(widgetName);
                }
            }
        });

        $.post(
            'dashboard/update',
            {
                type: 'widgets',
                options: widgetsDataUpdate
            }
        ).done(function () {
            $.get(
                'dashboard/get-data',
                { widgets: widgetNames, dashboard: dashboard.getName() }
            ).done(function (response) {
                if (response.success) {
                    for (var widgetName in response.data) {
                        if (response.data.hasOwnProperty(widgetName)) {
                            var newData = response.data[widgetName];
                            widgetName = widgetName.replace('-', '_');
                            dashboard.getWidget(widgetName).refreshData(newData);
                        }
                    }
                }
                processLoad(false);
            });
        })
    }

    function updatePositions() {
        $elem.find('.widgets-container').children('.widget').each(function (i, el) {
            blockOptions.positions[el.id] = i;
        });
    }

    function initialOptionsForNewWidgets(widgetName) {
        if (parseInt(blockOptions.interval) !== 0) {
            Widget.setState(widgetName, 'interval', blockOptions.interval);
        }
    }

    function sendStateToServer() {
        processLoad(true);
        $.post(
            'dashboard/update',
            { type: 'block', options: blockOptions, dashboard: dashboard.getName() }
        ).done(function (res) {
            processLoad(false);
        });
    }

    function getElem() {
        if (!$elem) render();
        return $elem;
    }

    function render(html) {
        $elem = html;
        setSettingsListeners();
    }

    function processLoad(load) {
        var loader = $elem.find('.block-loader-bg');
        var settingButton = $elem.find('.block-settings-button');

        if (load) {
            loader.removeClass('hidden');
            settingButton.find('.fa-gear').addClass('fa-spin');
            settingButton.addClass('disabled');
        } else {
            loader.addClass('hidden');
            settingButton.find('.fa-gear').removeClass('fa-spin');
            settingButton.removeClass('disabled');
        }
    }

    // changes the menu position if not fit
    function menuPosition() {
        var menu = $elem.find('.menu-add-widget')[0];
        var blockPosition = $elem.position();
        var toBotom = $(document).find('.content-wrapper').height() - blockPosition.top;
        var menuHeight = $(menu).height() - $(menu).parent().height();
        if (toBotom < menuHeight + $(menu).parent().parent().height() - $elem.height() + $(menu).parent().height()) {
            $(menu).css({ 'top': '-' + menuHeight + 'px' });
        } else {
            $(menu).css({ 'top': '0' });
        }
    }

    function editMode(edit) {
        if (edit) {
            $elem.find('.settings').removeClass('hidden');
            $elem.addClass('edited');
        } else {
            $elem.find('.settings').addClass('hidden');
            $elem.removeClass('edited');
        }
    }

    this.getElem = getElem;
}
