﻿$(document).ready(function () {
    fixBootstrapModalBodyPadding();
    openFormTabWithError();

    // Fix body's right padding expanding after closing a Bootstrap 3 modal
    function fixBootstrapModalBodyPadding() {
        $('.modal').on('hidden.bs.modal', function (e) {
            e.stopPropagation();
            $('body').css('padding-right', '');
        });
    }

    // Open form tab with errors
    function openFormTabWithError() {
        var form = $('form')[0];
        if (!form) return;

        var tabsBlock = $('.nav-tabs')[0];
        if (!tabsBlock) return;

        var tabContentBlock = $('.tab-content')[0];
        var elementWithError = $(tabContentBlock).find('.has-error')[0];
        if (!elementWithError) return;

        var idTabPane = $(elementWithError).parents('.tab-pane')[0].id;
        var link = $(tabsBlock).find('a[href="#' + idTabPane + '"]');
        setTimeout(function () {
            link.click();
        }, 200);
    }
});