﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;

namespace WebAdmin.Controllers
{
    public class AdminController : Controller
    {
        #region DashBoard
        public ActionResult DashBoard()
        {
            return View();
        }
        #endregion

        #region Configuration
        #region System
        public ActionResult SystemLog()
        {
            SystemConfig objConfig = new SystemConfig();
            SystemConfigList model = objConfig.GetSystemLog();
            return View(model);
        }
        public ActionResult System()
        {
            SystemConfig objConfig = new SystemConfig();
            SystemConfig model = objConfig.GetSystemConfig(0);
            return View(model);
        }
        [HttpPost]
        public ActionResult System_MainConfig(SystemConfig model)
        {
            SystemConfig objConfig = new SystemConfig();
            if (Request.Form["btnMain"] != null)
            {
                model.CustomOptions = null; model.Rules = null;
                model.Hosts = null; model.EndPoints = null;
                model.Action = "Add";
                objConfig = objConfig.AddSystemConfig(model);
            }
            else if (Request.Form["revertMain"] != null)
            {
                objConfig = objConfig.RevertSystemConfig("MainConfig");
            }

            return RedirectToAction("System", "Admin");
        }
        [HttpPost]
        public ActionResult System_CustomOptions(SystemConfig model)
        {
            SystemConfig objConfig = new SystemConfig();
            if (Request.Form["btnCustom"] != null)
            {
                model.MainConfig = ""; model.Rules = "";
                model.Hosts = ""; model.EndPoints = "";
                model.Action = "Add";
                objConfig = objConfig.AddSystemConfig(model);
            }
            else if (Request.Form["revertCustom"] != null)
            {
                objConfig = objConfig.RevertSystemConfig("CustomOptions");
            }
            return RedirectToAction("System", "Admin");
        }
        [HttpPost]
        public ActionResult System_Rules(SystemConfig model)
        {
            SystemConfig objConfig = new SystemConfig();
            if (Request.Form["btnRules"] != null)
            {
                model.MainConfig = ""; model.CustomOptions = "";
                model.Hosts = ""; model.EndPoints = "";
                model.Action = "Add";
                objConfig = objConfig.AddSystemConfig(model);
            }
            else if (Request.Form["revertRules"] != null)
            {
                objConfig = objConfig.RevertSystemConfig("Rules");
            }
            return RedirectToAction("System", "Admin");
        }
        [HttpPost]
        public ActionResult System_Hosts(SystemConfig model)
        {
            SystemConfig objConfig = new SystemConfig();
            if (Request.Form["btnHosts"] != null)
            {
                model.MainConfig = ""; model.CustomOptions = "";
                model.Rules = ""; model.EndPoints = "";
                model.Action = "Add";
                objConfig = objConfig.AddSystemConfig(model);
            }
            else if (Request.Form["revertHosts"] != null)
            {
                objConfig = objConfig.RevertSystemConfig("Hosts");
            }
            return RedirectToAction("System", "Admin");
        }
        [HttpPost]
        public ActionResult System_EndPoints(SystemConfig model)
        {
            SystemConfig objConfig = new SystemConfig();
            if (Request.Form["btnEndPoints"] != null)
            {
                model.MainConfig = ""; model.CustomOptions = "";
                model.Rules = ""; model.Hosts = "";
                model.Action = "Add";
                objConfig = objConfig.AddSystemConfig(model);
            }
            else if (Request.Form["revertEndPoints"] != null)
            {
                objConfig = objConfig.RevertSystemConfig("EndPoints");
            }
            return RedirectToAction("System", "Admin");
        }
        #endregion

        #region Language
        public ActionResult LanguageList()
        {
            Language objConfig = new Language();
            LanguageList model = objConfig.GetLanguageList();
            return View(model);
        }
        public ActionResult LanguageAdd(int id = 0)
        {
            Language objConfig = new Language();
            Language model = new Language();
            if (id != 0)
            {
                model = objConfig.GetLanguage(id);
            }
            else
            {
                model.Id = 0;
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult LanguageAdd(Language model)
        {
            Language objLang = new Language();
            if (model.Id != 0)
            {
                model.Action = "Update";
            }
            else
            {
                model.Action = "Add";
            }
            if (model.IsActive != null)
            {
                model.IsActive = "1";
            }
            else
            {
                model.IsActive = "0";
            }
            objLang = objLang.AddLanguage(model);
            return RedirectToAction("LanguageList", "Admin");
        }
        #endregion
        public ActionResult Localization()
        {
            return View();
        }

        #region
        public ActionResult CountryList()
        {
            Country objConfig = new Country();
            CountryList model = objConfig.GetCountryList();
            return View(model);
        }
        public ActionResult CountryAdd(int id = 0)
        {
            Country objConfig = new Country();
            Country model = new Country();
            if (id != 0)
            {
                model = objConfig.GetCountry(id);
            }
            else
            {
                model.Id = 0;
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult CountryAdd(Country model)
        {
            Country obj = new Country();
            if (model.Id != 0)
            {
                model.Action = "Update";
            }
            else
            {
                model.Action = "Add";
            }
            if (model.RegEnabled != null)
            {
                model.RegEnabled = "1";
            }
            else
            {
                model.RegEnabled = "0";
            }
            if (model.LoginEnabled != null)
            {
                model.LoginEnabled = "1";
            }
            else
            {
                model.LoginEnabled = "0";
            }
            obj = obj.AddCountry(model);
            return RedirectToAction("CountryList", "Admin");
        }
        #endregion

        public ActionResult ClassificationList()
        {
            return View();
        }
        public ActionResult ClassificationAdd()
        {
            return View();
        }
        public ActionResult CurrencyList()
        {
            return View();
        }
        public ActionResult CurrencyAdd()
        {
            return View();
        }
        public ActionResult ExchangeRateList()
        {
            return View();
        }
        public ActionResult ExchangeRateAdd()
        {
            return View();
        }
        public ActionResult ExchangeRateSettings()
        {
            return View();
        }
        public ActionResult FastMessageList()
        {
            return View();
        }
        public ActionResult FastMessageAdd()
        {
            return View();
        }
        public ActionResult PlayerLevelList()
        {
            return View();
        }
        public ActionResult PlayerLevelAdd()
        {
            return View();
        }
        public ActionResult PlayerLevelSettings()
        {
            return View();
        }
        #endregion

        #region Server
        public ActionResult ServerList()
        {
            return View();
        }
        public ActionResult ServerAdd()
        {
            return View();
        }
        public ActionResult LobbyChat()
        {
            return View();
        }
        public ActionResult MaintenanceMode()
        {
            return View();
        }
        #endregion

        #region Skin
        public ActionResult SkinList()
        {
            return View();
        }
        public ActionResult SkinAdd()
        {
            return View();
        }
        public ActionResult SkinMoneyReport()
        {
            return View();
        }
        #endregion

        #region Affiliate
        public ActionResult AffiliateList()
        {
            return View();
        }
        public ActionResult AffiliateAdd()
        {
            return View();
        }
        public ActionResult AffiliateEdit()
        {
            return View();
        }
        public ActionResult AffiliateInfo()
        {
            return View();
        }
        public ActionResult AffiliateSettings()
        {
            return View();
        }
        public ActionResult AffiliateMake()
        {
            return View();
        }
        public ActionResult PlayerTransactionList()
        {
            return View();
        }
        public ActionResult PlayerTournamentsList()
        {
            return View();
        }
        public ActionResult PlayerTableList()
        {
            return View();
        }
        public ActionResult PlayerSessionList()
        {
            return View();
        }
        #endregion

        #region User
        public ActionResult UserDashBoard()
        {
            return View();
        }
        public ActionResult UserAdd()
        {
            return View();
        }
        public ActionResult PlayerDeviceList()
        {
            return View();
        }
        public ActionResult PlayerGameInfo()
        {
            return View();
        }
        public ActionResult PlayerGameSummary()
        {
            return View();
        }
        public ActionResult NotApprovedAvatar()
        {
            return View();
        }
        public ActionResult ParticipationList()
        {
            return View();
        }

        public ActionResult GroupPlayerList()
        {
            return View();
        }
        public ActionResult GroupPlayerAdd()
        {
            return View();
        }
        public ActionResult GroupPlayerEdit()
        {
            return View();
        }
        public ActionResult GroupPlayerView()
        {
            return View();
        }
        public ActionResult PredefinedAvatar()
        {
            return View();
        }
        public ActionResult RegisteredUsers()
        {
            return View();
        }
        public ActionResult UserList()
        {
            return View();
        }
        public ActionResult UserEdit()
        {
            return View();
        }
        public ActionResult UserPlayerTransactionList()
        {
            return View();
        }
        public ActionResult UserPlayerTableList()
        {
            return View();
        }
        public ActionResult UserPlayerTournamentList()
        {
            return View();
        }
        public ActionResult UserPlayerSessionList()
        {
            return View();
        }
        public ActionResult PlayerClubList()
        {
            return View();
        }
        public ActionResult UserPlayerDeviceList()
        {
            return View();
        }
        public ActionResult UserRole()
        {
            return View();
        }
        #endregion

        #region Backoffice Users
        public ActionResult CustomRoleAdd()
        {
            return View();
        }
        public ActionResult CustomRoleList()
        {
            return View();
        }
        public ActionResult AssignBackOfficeUser()
        {
            return View();
        }
        public ActionResult BackOfficeUserList()
        {
            return View();
        }
        public ActionResult BackOfficeUserRolePermissionAdd()
        {
            return View();
        }
        public ActionResult BackOfficeUserRolePermissionList()
        {
            return View();
        }
        #endregion

        #region Real Time Monitoring
        public ActionResult AllTablesChatList()
        {
            return View();
        }
        public ActionResult OnlinePlayerList()
        {
            return View();
        }
        public ActionResult OnlinePlayerLocation()
        {
            return View();
        }
        #endregion

        #region Affiliate Money Reports
        public ActionResult AffiliateProfit()
        {
            return View();
        }
        public ActionResult PlayerInsuranceReport()
        {
            return View();
        }
        public ActionResult PlayerReport()
        {
            return View();
        }

        #endregion

        #region Tables
        public ActionResult ActiveTables()
        {
            return View();
        }
        public ActionResult TableDashboard()
        {
            return View();
        }
        public ActionResult GameInfo()
        {
            return View();
        }
        public ActionResult GameList()
        {
            return View();
        }
        public ActionResult RentReport()
        {
            return View();
        }
        public ActionResult TableChats()
        {
            return View();
        }
        public ActionResult TableAdd()
        {
            return View();
        }
        public ActionResult TableHistory()
        {
            return View();
        }
        public ActionResult TableInfo()
        {
            return View();
        }
        public ActionResult TableList()
        {
            return View();
        }
        #endregion

        #region Tournaments
        public ActionResult ActiveTournaments()
        {
            return View();
        }
        public ActionResult AddTicketTemplate()
        {
            return View();
        }
        public ActionResult TournamentDashboard()
        {
            return View();
        }
        public ActionResult HiddenTicketTemplates()
        {
            return View();
        }
        public ActionResult SpinList()
        {
            return View();
        }
        public ActionResult TournamentGames()
        {
            return View();
        }
        public ActionResult TournamentTables()
        {
            return View();
        }
        public ActionResult TournamentTickets()
        {
            return View();
        }
        public ActionResult TournamentTableList()
        {
            return View();
        }
        public ActionResult TournamentAdd()
        {
            return View();
        }
        public ActionResult TournamentHistory()
        {
            return View();
        }
        public ActionResult TournamentInfo()
        {
            return View();
        }
        public ActionResult TournamentsStatistic()
        {
            return View();
        }
        public ActionResult TournamentsList()
        {
            return View();
        }
        #endregion

        #region Jackpot
        public ActionResult JackPotAwardsHistory()
        {
            return View();
        }
        public ActionResult Jackpot()
        {
            return View();
        }
        #endregion

        #region Transactions
        public ActionResult CreateTransaction()
        {
            return View();
        }
        public ActionResult TransactionDashboard()
        {
            return View();
        }
        public ActionResult ListCashOutPending()
        {
            return View();
        }
        public ActionResult ListPending()
        {
            return View();
        }
        public ActionResult PlayerTransactions()
        {
            return View();
        }
        public ActionResult ProcessedTransaction()
        {
            return View();
        }
        public ActionResult SystemTransactions()
        {
            return View();
        }
        public ActionResult TransactionInfo()
        {
            return View();
        }


        #endregion

        #region Financial Reports
        public ActionResult AdHocReport()
        {
            return View();
        }
        public ActionResult JackpotReport()
        {
            return View();
        }
        public ActionResult MoneyInSystem()
        {
            return View();
        }
        public ActionResult PlayerFinanceInsuranceReport()
        {
            return View();
        }
        public ActionResult PlayerMoney()
        {
            return View();
        }
        public ActionResult PlayerRake()
        {
            return View();
        }
        public ActionResult TableRake()
        {
            return View();
        }

        public ActionResult Rake()
        {
            return View();
        }
        #endregion

        #region Report Generation
        public ActionResult AddReport()
        {
            return View();
        }
        public ActionResult ReportList()
        {
            return View();
        }
        public ActionResult ReportView()
        {
            return View();
        }
        #endregion

        #region Actions Log
        public ActionResult AllChanges()
        {
            return View();
        }
        #endregion

        #region Anti-Fraud
        public ActionResult Bot()
        {
            return View();
        }
        public ActionResult ChipDumping()
        {
            return View();
        }
        public ActionResult CollusionGamesList()
        {
            return View();
        }
        public ActionResult CollusionTournamentsGames()
        {
            return View();
        }
        public ActionResult FraudIPList()
        {
            return View();
        }
        public ActionResult GameSearch()
        {
            return View();
        }
        public ActionResult NewPlayers()
        {
            return View();
        }
        public ActionResult TeamBlockList()
        {
            return View();
        }
        public ActionResult TournamentsChipDumping()
        {
            return View();
        }
        #endregion

        #region Banner Management
        public ActionResult AddBannerPlace()
        {
            return View();
        }
        public ActionResult BannerPlaceList()
        {
            return View();
        }
        public ActionResult BannerAdd()
        {
            return View();
        }
        public ActionResult BannerList()
        {
            return View();
        }
        #endregion 

        #region Casino Games
        public ActionResult CasinoGames()
        {
            return View();
        }
        public ActionResult CategoriesList()
        {
            return View();
        }
        public ActionResult ProviderList()
        {
            return View();
        }
        public ActionResult Settings()
        {
            return View();
        }

        #endregion

        #region Clubs Management
        public ActionResult ClubAdd()
        {
            return View();
        }
        public ActionResult ClubList()
        {
            return View();
        }
        public ActionResult UnionList()
        {
            return View();
        }
        #endregion

        #region Deposit Bonus
        public ActionResult BonusConfig()
        {
            return View();
        }
        public ActionResult BonusList()
        {
            return View();
        }

        #endregion

        #region Google Analytics
        public ActionResult DevicesReports()
        {
            return View();
        }
        public ActionResult G_Analytics()
        {
            return View();
        }
        #endregion

        #region In-App Purchases
        public ActionResult ProductPurchaseSettings()
        {
            return View();
        }
        public ActionResult ProductAdd()
        {
            return View();
        }
        public ActionResult ProductList()
        {
            return View();
        }
        public ActionResult PurchaseAdd()
        {
            return View();
        }
        public ActionResult PurchaseList()
        {
            return View();
        }
        #endregion

        #region Notification Management
        public ActionResult AddEmailTemplate()
        {
            return View();
        }
        public ActionResult EmailTemplateList()
        {
            return View();
        }
        public ActionResult NotificationAdd()
        {
            return View();
        }
        public ActionResult NotificationList()
        {
            return View();
        }
        #endregion

        #region Payment Systems
        public ActionResult Bitcoin()
        {
            return View();
        }
        public ActionResult DefaultLimitValues()
        {
            return View();
        }
        public ActionResult EasyPayDirectPayment()
        {
            return View();
        }
        public ActionResult EcorePay()
        {
            return View();
        }
        public ActionResult FastPay()
        {
            return View();
        }
        public ActionResult LimitSettings()
        {
            return View();
        }
        public ActionResult LiteCoin()
        {
            return View();
        }
        public ActionResult PaymentLimitsList()
        {
            return View();
        }
        public ActionResult PaymentSettings()
        {
            return View();
        }
        #endregion

        #region Prohibited Users
        public ActionResult AddProhibitedDevice()
        {
            return View();
        }
        public ActionResult AddProhibitedIP()
        {
            return View();
        }
        public ActionResult ProhibitedDeviceList()
        {
            return View();
        }
        public ActionResult ProhibitedIPList()
        {
            return View();
        }
        #endregion

        #region Promotions Management
        public ActionResult AddPromotion()
        {
            return View();
        }
        public ActionResult Promotions()
        {
            return View();
        }
        #endregion

        #region Rakeback
        public ActionResult RakebackPercentConfig()
        {
            return View();
        }
        public ActionResult RakebackReport()
        {
            return View();
        }
        #endregion

        #region Redeem Codes
        public ActionResult AddRedeemCode()
        {
            return View();
        }
        public ActionResult GenerateRedeemCode()
        {
            return View();
        }
        public ActionResult RedeemCodeList()
        {
            return View();
        }
        #endregion

        #region Reports
        public ActionResult ExportReportList()
        {
            return View();
        }

        #endregion
    }
}