﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin.Controllers
{
    public class SharedController : Controller
    {
        #region
        [ChildActionOnly]
        public ActionResult _AdminHeader()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult _AdminLeftMenu()
        {
            return View();
        }
        #endregion
    }
}